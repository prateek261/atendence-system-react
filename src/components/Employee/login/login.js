import React, {useState} from 'react';
import {NavLink} from 'react-router-dom';
import axios from 'axios';
import {successAlert, errorAlert} from '../../Admin/utils/alert';
import Loader from '../../Admin/utils/loader';
import logo from '../../../assets/img/logo.png';
import {serverUrl, headers, tokenHeaders} from '../../Admin/utils/ajaxHeaders';

const EmpLogin = ({match})=>{
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [loader, setLoader] = useState(false)

  const formSubmit = async (e)=>{
    e.preventDefault();
    setLoader(true)
    const payload = {
      email: email,
      password: password
    }

    axios.post(`${serverUrl}account-sign/employee-login/`, payload, {headers: headers})
    .then(json=>{
      if(json && json.data && json.data.message==="logged in"){
        localStorage.setItem('user', JSON.stringify(json.data))
        localStorage.setItem('token', json.data.token)
        setEmail('')
        setPassword('')
        setLoader(false)
        window.location.href= '/employee/dashboard'
      }else if(json && json.message){
        alert(json.message)
      }
    })
    .catch(err=>{
      setLoader(false)
      alert('try again')
    })
  }
  return(
      <article>
      {loader?
        <Loader />
      :null}
          <section className="sectionColumns">
            <div className="leftColumn">
                <div className="content-leftColumn">
                  <div className="columnImg" style={{padding: '20%'}}>
                    <img src={logo} alt="sideimg"/>
                  </div>
                    <div className="container">

                    </div>
                </div>
            </div>
              <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4 loginHeader">Employee Login</h5>
                      <div className="boxComman">
                          <form onSubmit={formSubmit}>
                              <div className="form-group">
                                  <label>Email address</label>
                                  <input type="email" className="form-control" required onChange={(e)=>{
                                    setEmail(e.target.value)
                                  }} value={email} placeholder="Enter Email Address"/>
                              </div>
                              <div className="form-group">
                                  <label>Password</label>
                                  <div className="inputPassword">
                                      <input type="password" required onChange={(e)=>{
                                        setPassword(e.target.value)
                                      }} value={password} className="form-control" placeholder="Enter Password"/>
                                  </div>
                              </div>
                              <div className="row align-items-center">
                                  <div className="col-md-6">
                                      <p className="mb-0"><NavLink to="/forgetpassword" title="Forgot Password?" className="font-weight-medium loginColor"> Forgot Password?</NavLink> </p>
                                  </div>
                                  <div className="col-md-6 text-right">
                                      <input type="submit" title="Login" value="Login"/>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
}

export default EmpLogin;
