import React from 'react';
import { Redirect } from 'react-router';

const EmpLogout = () =>{
  localStorage.removeItem('token')
  return(
    <Redirect to="/"/>
  );
}

export default EmpLogout;
