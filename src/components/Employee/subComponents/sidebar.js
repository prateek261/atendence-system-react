import React, {useState} from 'react';
import {NavLink} from 'react-router-dom';

const MainSidebar = (props)=>{
  const [requests, setRequests] = useState(false)
  return(
    <div className="rightColumnMenu">
      <ul className="menuRightColumn">
          <li><NavLink to="/employee/dashboard" title="Dashboard">Dashboard</NavLink></li>
          <li><NavLink to="/employee/calendar" title="employee">Calendar</NavLink></li>
          <li><NavLink to="/employee/details" title="employee">Profile</NavLink></li>
          <li><NavLink to="/employee/monthly-report" title="monthlyresult">Monthly Report</NavLink></li>
          <li><a href="#a" onClick={(e)=>{
            e.preventDefault();
            setRequests(!requests)
          }}>Requests &nbsp;
          {requests?
            <i className="fa fa-angle-down" aria-hidden="true"></i>
          : <i className="fa fa-angle-up" aria-hidden="true"></i>}
          </a>
            {requests?
              <ul style={{marginLeft: '25px'}}>
                <li><NavLink to="/employee/requests/leave">Leave Requests</NavLink></li>
              </ul>
            :null}
          </li>
      </ul>
    </div>
  )
}

export default MainSidebar;
