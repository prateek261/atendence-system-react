import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../../../Admin/utils/ajaxHeaders';
import './email.css';
import Datetime from 'react-datetime'
import Loader from '../../../Admin/utils/loader';

const initial = {
  profile: '',
  start_date: '',
  end_date: '',
  request_type: '',
  body: ''
}

export const getProfile = ()=>{
  try {
    let user = localStorage.getItem('user')
    user = JSON.parse(user)
    return user.data
  } catch (e) {
    return null
  }
}

const EmailModel = ({ close, title, refreshData })=>{
  const [request, setRequest] = useState(initial)
  const [requestType, setRequestType] = useState([])

  const [loader, setLoader] = useState(false)

  useEffect(()=>{
    const emp = getProfile()
    setRequest({
      ...request,
      profile: emp.id
    })
  }, [])

  useEffect(()=>{
    axios.get(`${serverUrl}configure/choice/REQUEST_TYPE/`, {headers: tokenHeaders})
    .then(res=>{
      setRequestType(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  const sendEmail = (e)=>{
    e.preventDefault()
    setLoader(true)
    axios.post(`${serverUrl}request/data/`, request, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        refreshData()
        setLoader(false)
        alert(res.data.message)
        close()
      }else {
        alert(res.data.message)
      }
    })
    .catch(err=>{
      setLoader(false)
      alert('try again')
      console.log(err);
    })
  }
  return(
    <div className="fixedEmail card">
      {loader?
        <Loader />
      :null}
      <form method="POST" onSubmit={ sendEmail }>
        <div className="card-header">{title}
          <span className="close" onClick={()=>close()}>&times;</span>
        </div>
        <div className="card-body">
          <div className="form-group">
            <label>Subject</label>
            <select onChange={(e)=> setRequest({
              ...request,
              request_type: e.target.value
            })} value={request.request_type} className="form-control">
              <option value="">Select</option>
              {requestType && requestType.length>0?
                requestType.map((e, i)=>(
                  <option key={ i }>{ e[1] }</option>
                ))
              :null}
            </select>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label>Start Date</label>
                <Datetime
                  dateFormat="DD/MM/YY"
                  utc={true}
                  onChange={(e)=>{
                    setRequest({
                      ...request,
                      start_date: e
                    })
                  }}
                  value={ request.start_date }
                  required={ true }
                />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label>End Date</label>
                <Datetime
                  dateFormat="DD/MM/YY"
                  utc={true}
                  onChange={(e)=>{
                    setRequest({
                      ...request,
                      end_date: e
                    })
                  }}
                  value={ request.end_date }
                  required={ true }
                />
              </div>
            </div>
          </div>

          <div className="form-group">
            <label>Body</label>
            <textarea className="form-control" onChange={ (e)=>{
              setRequest({
                ...request,
                body: e.target.value
              })
            } } value={ request.body }></textarea>
          </div>
          <div className="form-group">
            <input type="submit" value="Send" className="btn" />
            <button className="btn" onClick={()=> close()} style={{marginLeft: '15px'}}>Close</button>
          </div>
        </div>
      </form>
    </div>
  )
}
export default EmailModel;
