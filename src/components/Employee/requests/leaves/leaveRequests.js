import React, {useState, useEffect} from 'react';
import DataTable from 'react-data-table-component';
import Header from '../../subComponents/header';
import Path from '../../subComponents/path';
import MainSidebar from '../../subComponents/sidebar';
import axios from 'axios';
import {serverUrl, tokenHeaders, getId} from '../../../Admin/utils/ajaxHeaders';
import moment from 'moment';

import EmailModel from '../subComponents/email';

const columns = [
  {
    name: 'Start Date time',
    selector: 'start_date',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{moment(row.start_date, "YYYY-MM-DD hh:mm").format("DD-MMM-YYYY hh:mm A")}</span>
  },
  {
    name: 'End Date time',
    selector: 'end_date',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{moment(row.end_date, "YYYY-MM-DD hh:mm").format("DD-MMM-YYYY hh:mm A")}</span>
  },
  {
    name: 'Request Type',
    selector: 'request_type',
    sortable: true,
  },
  {
    name: 'Accepted',
    selector: 'accepted',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999', fontSize: '13px'}} className={`badge ${row.accepted === 'ACCEPTED'? "badge-success" : row.accepted === 'PENDING'? "badge-primary": "badge-danger"}`}>{row.accepted}</span>
  },
]

const EmpLeaveRequests = ({match})=>{
  const path = [{name: 'Requests', url: '/employee/requests/leave'},
                {name: 'Leave', url: '/employee/requests/leave'}]
  const [requests, setRequests] = useState([])
  const [emailModel, setEmailModel] = useState(false)

  const user_id = getId()

  useEffect(()=>{
    getallRequests()
  }, [])

  const getallRequests = ()=>{
    axios.get(`${serverUrl}request/data/${user_id}/`, {headers: tokenHeaders})
    .then(res=>{
      setRequests(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }

  return(
    <article>
      {emailModel?
        <EmailModel
          title="Create New Leave Request"
          close={()=> setEmailModel(false)}
          refreshData={ getallRequests }
        />
      :null}
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <div>
                    <span style={{color: '#0056b3', cursor: 'pointer'}} onClick={()=> setEmailModel(true)}>Create New Request</span>
                    <hr/>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 col-md-12">
                      <DataTable
                        paginationRowsPerPageOptions={[10,25,50,100]}
                        pagination={true}
                        columns={columns}
                        data={requests}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>

  )
}
export default EmpLeaveRequests;
