import React, {useState, useEffect} from 'react';
import {
  Calendar,
  DateLocalizer,
  momentLocalizer,
  move,
  Views,
  Navigate,
  components,
} from 'react-big-calendar'
import moment from 'moment'
import Header from '../../subComponents/header';
import Path from '../../subComponents/path';
import MainSidebar from '../../subComponents/sidebar';
import './calendar.css';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../../../Admin/utils/ajaxHeaders';

const localizer = momentLocalizer(moment)

const EmpBigCalendar = props => {
  const path = [{name: 'Calendar', url: '/employee/calendar'}]
  const [eventList, setEventList] = useState([])

  useEffect(()=>{
    axios.get(`${serverUrl}daydate/date-data/`, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.length>0){
        processEvent(res.data)
      }
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  const processEvent = (arr)=>{
    const events = []
    arr.map((e, i)=>{
      const date = e.date.split("-")
      events.push({
        id: e.id,
        title: e.day_type,
        start: new Date(date[0], parseInt(date[1]-1), date[2]),
        end: new Date(date[0], parseInt(date[1]-1), date[2]),
        allDay: true
      })
    })
    setEventList(events)
  }

  return(
    <article>
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <div className="row">
                    <div className="col-lg-12 col-md-12">
                      <Calendar
                        selectable
                        localizer={localizer}
                        defaultDate={new Date()}
                        views={['month', 'work_week', 'day', 'agenda']}
                        defaultView={Views.MONTH}
                        step={15}
                        showMultiDayTimes
                        min={new Date(2019, 6, 6, 9, 30, 0)}
                        max={new Date(2019, 6, 6, 19, 0, 0)}
                        events={eventList}
                        startAccessor="start"
                        endAccessor="end"
                      />
                    </div>
                  </div>
                  <br/><br/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>

  )
}
export default EmpBigCalendar;
