import React, {useState, useEffect} from 'react';
import Header from '../subComponents/header';
import Path from '../subComponents/path';
import MainSidebar from '../subComponents/sidebar';
import Datetime from 'react-datetime';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../../Admin/utils/ajaxHeaders';
import { NavLink } from 'react-router-dom'
import Loader from '../../Admin/utils/loader';

import DocumentModel from '../../Admin/employees/DocumentModel'
import ResetPasswordModel from '../../Admin/employees/ResetPasswordModel'
import { form } from './JoiningForm/form'

export const getProfile = ()=>{
  try {
    let user = localStorage.getItem('user')
    user = JSON.parse(user)
    return user.data
  } catch (e) {
    return null
  }
}

const EmpDetails = ({match})=>{
  const path = [{name: 'Profile', url: '/employee/details'}]
  const [employee, setEmployee] = useState(form)
  const emp = getProfile()

  const [docModel, setDocModel] = useState(false)
  const [resetModel, setResetModel] = useState(false)
  const [loader, setLoader] = useState(false)

  useEffect(()=>{
    if(emp && emp.id){
      axios.get(`${serverUrl}profile/data/${emp.id}/`, {headers: tokenHeaders})
      .then(res=>{
        setEmployee(res.data[0])
      })
      .catch(err=>{
        console.log(err);
      })
    }
  }, [emp])

  const updateEmployee = (e)=>{
    e.preventDefault()
    setLoader(true)
    axios.put(`${serverUrl}profile/update-data/${employee.id}/`, employee, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        alert(res.data.message)
      }else {
        alert(res.data.message)
      }
      setLoader(false)
    })
    .catch(err=>{
      setLoader(false)
      console.log(err);
      alert(err)
    })
  }
  return(
    <article>
      {loader?
        <Loader />
      :null}
      {docModel?
        <DocumentModel
          id={ employee.id }
          close={ ()=> setDocModel(false) }
        />
      :null}
      {resetModel?
        <ResetPasswordModel
          close={ ()=> setResetModel(false) }
          email={ employee.email }
        />
      :null}
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                <form method="POST" onSubmit={updateEmployee}>
                  <div className="row">
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>First Name</label>
                        <input type="text" required className="form-control" onChange={(e)=> setEmployee({...employee, first_name: e.target.value})} value={employee.first_name} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Middle Name</label>
                        <input type="text" className="form-control" onChange={(e)=> setEmployee({...employee, middle_name: e.target.value})} value={employee.middle_name? employee.middle_name: ''} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Last Name</label>
                        <input type="text" required className="form-control" onChange={(e)=> setEmployee({...employee, last_name: e.target.value})} value={employee.last_name} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Date Of Birth</label>
                        <Datetime
                          dateFormat="YYYY-MM-DD"
                          timeFormat={false}
                          utc={true}
                          onChange={(e)=>{
                            setEmployee({...employee, dob:e.format("YYYY-MM-DD")})
                          }} value={employee.dob}
                          required={true}
                        />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Email</label>
                        <input type="email" required className="form-control" onChange={(e)=> setEmployee({...employee, email:e.target.value})} value={employee.email} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Phone No.</label>
                        <input type="number" required className="form-control" onChange={(e)=> setEmployee({...employee, phone:e.target.value})} value={employee.phone} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Employee Code</label>
                        <input type="text" readOnly required className="form-control" onChange={(e)=> setEmployee({...employee, code:e.target.value})} value={employee.code} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Aadhar Card</label>
                        <input type="text" className="form-control" onChange={(e)=> setEmployee({...employee, aadhar_card:e.target.value})} value={employee.aadhar_card || ''} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Pan Card</label>
                        <input type="text" className="form-control" onChange={(e)=> setEmployee({...employee, pan_card:e.target.value})} value={employee.pan_card} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Available Sick Leaves</label>
                        <input type="text" readOnly className="form-control" onChange={(e)=> setEmployee({...employee, avail_sick:e.target.value})} value={employee.avail_sick} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Available Casual Leaves</label>
                        <input type="text" readOnly className="form-control" onChange={(e)=> setEmployee({...employee, avail_casual:e.target.value})} value={employee.avail_casual || ''} />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4">
                      <div className="form-group">
                        <label>Personal Email Password</label>
                        <input type="password" className="form-control" onChange={(e)=> setEmployee({...employee, email_password:e.target.value})} value={employee.email_password} />
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <input type="submit" value="Save Changes" className="btn" />
                    <NavLink to="/employee/details/edit" style={{marginLeft: '15px'}}>Edit Full Profile</NavLink>
                    <a href="#a" style={{marginLeft: '15px'}} onClick={ ()=> setDocModel(true) }>Documents</a>
                    <a href="#a" style={{marginLeft: '15px'}} onClick={ ()=> setResetModel(true) }>Reset Password</a>
                  </div>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>
  )
}
export default EmpDetails;
