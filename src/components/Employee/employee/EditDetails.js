import React, { useState, useEffect } from 'react'
import Header from '../subComponents/header'
import Path from '../subComponents/path'
import MainSidebar from '../subComponents/sidebar'
import Datetime from 'react-datetime'
import axios from 'axios'
import {serverUrl, tokenHeaders} from '../../Admin/utils/ajaxHeaders'
import moment from 'moment'
import { NavLink } from 'react-router-dom'
import { form } from './JoiningForm/form'
import {
  TextInput,
  TextArea,
  DateInput
} from './JoiningForm/Controls/Controls'

import Passport from './JoiningForm/Passport'
import Relatives from './JoiningForm/Relatives'
import Address from './JoiningForm/Address'
import EmergencyContact from './JoiningForm/EmergencyContact'
import PersonalDetails from './JoiningForm/PersonalDetails'
import EmployeeHistory from './JoiningForm/EmployeeHistory'
import Education from './JoiningForm/Education'
import Training from './JoiningForm/Training'
import Certification from './JoiningForm/Certification'
import References from './JoiningForm/References'
import Loader from '../../Admin/utils/loader';

const getProfile = ()=>{
  try {
    let user = localStorage.getItem('user')
    user = JSON.parse(user)
    return user.data
  } catch (e) {
    return null
  }
}

const EditDetails = ({match})=>{
  const path = [
    {name: 'Profile', url: '/employee/details'},
    {name: 'Employee Data Form', url: '/employee/details/edit'}
  ]
  const emp = getProfile()
  const [employee, setEmployee] = useState(form)
  const [joiningForm, setJoiningForm] = useState(form)

  const [accept, setAccept] = useState(false)
  const [loader, setLoader] = useState(false)

  useEffect(()=>{
    if(emp && emp.id){
      axios.get(`${serverUrl}profile/data/${emp.id}/`, {headers: tokenHeaders})
      .then(res=>{
        setEmployee(res.data[0])
        const payload = {
          first_name: res.data[0].first_name,
          middle_name: res.data[0].middle_name,
          last_name: res.data[0].last_name,
          date_of_joining: moment(res.data[0].date_of_joining),
          job_title: res.data[0].job_title,
          dob: moment(res.data[0].dob),
          passport: jsonParser(res.data[0].passport),
          employment_visa_denied: jsonParser(res.data[0].employment_visa_denied),
          relatives: jsonParser(res.data[0].relatives),
          address: jsonParser(res.data[0].address),
          emergency_contact_inforation: jsonParser(res.data[0].emergency_contact_inforation),
          personal_details: jsonParser(res.data[0].personal_details),
          emplyee_history: jsonParser(res.data[0].emplyee_history),
          education: jsonParser(res.data[0].education),
          training: jsonParser(res.data[0].training),
          certification: jsonParser(res.data[0].certification),
          references: jsonParser(res.data[0].references),
        }
        setJoiningForm(payload)
      })
      .catch(err=>{
        console.log(err);
      })
    }
  }, [emp])

  const jsonParser = (str)=>{
    try {
      return JSON.parse(str)
    } catch (e) {
      return ''
    }
  }

  const updateEmployee = (e)=>{
    e.preventDefault()
    setLoader(true)
    const payload = {
      first_name: joiningForm.first_name,
      middle_name: joiningForm.middle_name,
      last_name: joiningForm.last_name,
      date_of_joining: joiningForm.date_of_joining.format("YYYY-MM-DD"),
      job_title: joiningForm.job_title,
      dob: joiningForm.dob.format("YYYY-MM-DD"),
      passport: JSON.stringify(joiningForm.passport),
      employment_visa_denied: JSON.stringify(joiningForm.employment_visa_denied),
      relatives: JSON.stringify(joiningForm.relatives),
      address: JSON.stringify(joiningForm.address),
      emergency_contact_inforation: JSON.stringify(joiningForm.emergency_contact_inforation),
      personal_details: JSON.stringify(joiningForm.personal_details),
      emplyee_history: JSON.stringify(joiningForm.emplyee_history),
      education: JSON.stringify(joiningForm.education),
      training: JSON.stringify(joiningForm.training),
      certification: JSON.stringify(joiningForm.certification),
      references: JSON.stringify(joiningForm.references),
    }


    axios.put(`${serverUrl}profile/update-data/${employee.id}/`, payload, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        alert(res.data.message)
      }else {
        alert(res.data.message)
      }
      setLoader(false)
    })
    .catch(err=>{
      setLoader(false)
      console.log(err);
      alert(err)
    })
  }

  return(
    <article>
      {loader?
        <Loader />
      :null}
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <form method="POST" onSubmit={ updateEmployee }>
                    <div className="row">
                      <div className="col-lg-4">
                        <TextInput
                          label="First Name"
                          type="text"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              first_name: e
                            })
                          } }
                          value={ joiningForm.first_name }
                          required={ true }
                        />
                      </div>
                      <div className="col-lg-4">
                        <TextInput
                          label="Middle Name"
                          type="text"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              middle_name: e
                            })
                          } }
                          value={ joiningForm.middle_name }
                          required={ false }
                        />
                      </div>
                      <div className="col-lg-4">
                        <TextInput
                          label="Last Name"
                          type="text"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              last_name: e
                            })
                          } }
                          value={ joiningForm.last_name }
                          required={ true }
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-4">
                        <DateInput
                          format="DD/MM/YY"
                          label="Date of Joining"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              date_of_joining: e
                            })
                          } }
                          value={ joiningForm.date_of_joining }
                          required={ true }
                        />
                      </div>
                      <div className="col-lg-4">
                        <TextInput
                          label="Job Title"
                          type="text"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              job_title: e
                            })
                          } }
                          value={ joiningForm.job_title }
                          required={ true }
                        />
                      </div>
                      <div className="col-lg-4">
                        <DateInput
                          format="DD/MM/YY"
                          label="Date of Birth"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              dob: e
                            })
                          } }
                          value={ joiningForm.dob }
                          required={ true }
                        />
                      </div>
                    </div>
                    <hr/>
                    <div className="row">
                      <div className="col-lg-4">
                        <Passport
                          getPassport={ (passport)=>{
                            setJoiningForm({
                              ...joiningForm,
                              passport: passport
                            })
                          } }
                          passportData={ joiningForm.passport }
                        />
                      </div>
                      <div className="col-lg-8">
                        <TextArea
                          label="Have your Employment Visa denied in any countries if yes detail the reason[s]"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              employment_visa_denied: e
                            })
                          } }
                          value={ joiningForm.employment_visa_denied }
                          required={ false }
                        />
                        <div className="row">
                          <div className="col-md-6">
                            <TextInput
                              label="Driving License"
                              type="text"
                              changeFunc={ (e)=>{
                                setJoiningForm({
                                  ...joiningForm,
                                  driving_license: e
                                })
                              } }
                              value={ joiningForm.driving_license }
                              required={ false }
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr/>
                    <Relatives
                      getRelatives={ (relatives)=>{
                        setJoiningForm({
                          ...joiningForm,
                          relatives,
                        })
                      } }
                      relativesData={ joiningForm.relatives }
                    />
                    <hr/>
                    <Address
                      getAddress={ (address)=>{
                        setJoiningForm({
                          ...joiningForm,
                          address,
                        })
                      } }
                      addressData={ joiningForm.address }
                    />
                    <hr/>
                    <EmergencyContact
                      eInfo={ joiningForm.emergency_contact_inforation }
                      getContact={ (emergency_contact_inforation)=>{
                        setJoiningForm({
                          ...joiningForm,
                          emergency_contact_inforation,
                        })
                      } }
                      infoData={ joiningForm.emergency_contact_inforation }
                    />
                    <hr/>
                    <PersonalDetails
                      detailsData={ joiningForm.personal_details }
                      getDetails={ (personal_details)=>{
                        setJoiningForm({
                          ...joiningForm,
                          personal_details,
                        })
                      } }
                    />
                    <hr/>
                    <EmployeeHistory
                      getHistory={ (emplyee_history)=>{
                        setJoiningForm({
                          ...joiningForm,
                          emplyee_history,
                        })
                      } }
                      historyData={ joiningForm.emplyee_history }
                    />
                    <hr/>
                    <Education
                      getEducation={ (education)=>{
                        setJoiningForm({
                          ...joiningForm,
                          education,
                        })
                      } }
                      educationData={ joiningForm.education }
                    />
                    <hr/>
                    <Training
                      getTraining={ (training)=>{
                        setJoiningForm({
                          ...joiningForm,
                          training,
                        })
                      }}
                      trainingData={ joiningForm.training }
                    />
                    <hr/>
                    <Certification
                      getCertification={ (certification)=>{
                        setJoiningForm({
                          ...joiningForm,
                          certification,
                        })
                      } }
                      certificationData={ joiningForm.certification }
                    />
                    <hr/>
                    <References
                      getReferences={ (references)=>{
                        setJoiningForm({
                          ...joiningForm,
                          references,
                        })
                      } }
                      referenceData={ joiningForm.references }
                    />
                    <p>
                    <input type="checkbox" onChange={ (e)=>{
                      if(e.target.checked){
                        setAccept(true)
                      }else {
                        setAccept(false)
                      }
                    } } value={ accept } />
                    &nbsp;The above details are true and best of my knowledage. I understand that any misrepresentation of facts may be called for disciplinary action.</p>
                    <div className="form-group">
                      <input type="submit" disabled={ !accept } value="Submit" className="btn" />
                      <NavLink to="/employee/details/" style={{marginLeft: '15px'}}>Back to Profile</NavLink>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>

  )
}
export default EditDetails
