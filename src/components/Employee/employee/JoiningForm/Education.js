import React, { useState, useEffect } from 'react'
import { educationForm } from './form'
import {
  TextInput,
  DateInput
} from './Controls/Controls'
import moment from 'moment'

const Education = ({ getEducation, educationData })=>{
  const [educationArr, setEducationArr] = useState([])

  useEffect(()=>{
    if(educationData && educationData.length>0){
      setEducationArr(educationData)
    }
  }, [educationData])

  const addForm = ({ setEducation })=>{
    setEducationArr([...educationArr, []])
  }

  const reset = ()=>{
    setEducationArr([])
    getEducation([])
  }

  const setData = (payload, form_key)=>{
    const temp = [...educationArr]
    temp[form_key] = payload
    setEducationArr(temp)
    getEducation(temp)
  }

  return(
    <div>
        <h6>
          Education Details <span onClick={ addForm } className="btn-link">Add New</span>
          &nbsp;<span onClick={ reset } className="btn-link text-danger">Reset</span>
        </h6>
        {educationArr && educationArr.length>0?
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Qualification</th>
                <th>University/Board</th>
                <th>Specialization</th>
                <th>Year of passing</th>
                <th>Grade/CGPA</th>
              </tr>
            </thead>
            <tbody>
              {educationArr.map((e, i)=>(
                <Form
                  key={ i }
                  form_key={ i }
                  setData={ setData }
                  form={ e && e.qualification? e : educationForm }
                />
              ))}
            </tbody>
          </table>
        :null}
      </div>
  )
}

const Form = ({ form_key, setData , form})=>{
  const [education, setEducation] = useState(form)

  const setter = (key, value)=>{
    const payload = {
      ...education,
      [key]: value
    }
    setEducation(payload)
    setData(payload, form_key)
  }

  return(
    <tr>
          <td>
            <TextInput
              type="text"
              changeFunc={ (e)=>{
                setter('qualification', e)
              } }
              value={ education.qualification }
              required={ true }
            />
          </td>
          <td>
            <TextInput
              type="text"
              changeFunc={ (e)=>{
                setter('university_board', e)
              } }
              value={ education.university_board }
              required={ true }
            />
          </td>
          <td>
            <TextInput
              type="text"
              changeFunc={ (e)=>{
                setter('specialization', e)
              } }
              value={ education.specialization }
              required={ true }
            />
          </td>
          <td>
            <DateInput
              format="YYYY"
              changeFunc={ (e)=>{
                setter('year_of_passing', e)
              } }
              value={ education.year_of_passing }
              required={ true }
            />
          </td>
          <td>
            <TextInput
              type="text"
              changeFunc={ (e)=>{
                setter('Grade_CGPA', e)
              } }
              value={ education.Grade_CGPA }
              required={ true }
            />
          </td>
        </tr>
  )
}

export default Education
