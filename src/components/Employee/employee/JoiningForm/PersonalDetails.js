import React, { useState, useEffect } from 'react'
import { form } from './form'
import {
  TextInput
} from './Controls/Controls'

const PersonalDetails = ({ getDetails, detailsData })=>{
  const [personalDetails, setPersonalDetails] = useState(form.personal_details)

  useEffect(()=>{
    if(detailsData && detailsData.martial_status){
      setPersonalDetails(detailsData)
    }
  }, [detailsData])

  useEffect(()=>{
    getDetails(personalDetails)
  }, [personalDetails])

  return(
    <div>
        <h6>Personal Details</h6>
        <div className="row">
          <div className="col-lg-4">
            <TextInput
              label="Martial Status"
              type="text"
              changeFunc={ (e)=>{
                setPersonalDetails({
                  ...personalDetails,
                  martial_status: e
                })
              } }
              value={ personalDetails.martial_status }
              required={ true }
            />
          </div>
          <div className="col-lg-4">
            <TextInput
              label="Spouse Name & Profession"
              type="text"
              changeFunc={ (e)=>{
                setPersonalDetails({
                  ...personalDetails,
                  spouse_name_and_profession: e
                })
              } }
              value={ personalDetails.spouse_name_and_profession }
              required={ true }
            />
          </div>
          <div className="col-lg-4">
            <TextInput
              label="No of Children"
              type="number"
              changeFunc={ (e)=>{
                setPersonalDetails({
                  ...personalDetails,
                  no_of_children: e
                })
              } }
              value={ personalDetails.no_of_children }
              required={ true }
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <TextInput
              label="Father's Name"
              type="text"
              changeFunc={ (e)=>{
                setPersonalDetails({
                  ...personalDetails,
                  father: {
                    ...personalDetails.father,
                    name: e
                  }
                })
              } }
              value={ personalDetails.father.name }
              required={ true }
            />
            <TextInput
              label="Father's Profession"
              type="text"
              changeFunc={ (e)=>{
                setPersonalDetails({
                  ...personalDetails,
                  father: {
                    ...personalDetails.father,
                    profession: e
                  }
                })
              } }
              value={ personalDetails.father.profession }
              required={ true }
            />
          </div>
          <div className="col-lg-6">
            <TextInput
              label="Mother's Name"
              type="text"
              changeFunc={ (e)=>{
                setPersonalDetails({
                  ...personalDetails,
                  mother: {
                    ...personalDetails.mother,
                    name: e
                  }
                })
              } }
              value={ personalDetails.mother.name }
              required={ true }
            />
            <TextInput
              label="Mother's Profession"
              type="text"
              changeFunc={ (e)=>{
                setPersonalDetails({
                  ...personalDetails,
                  mother: {
                    ...personalDetails.mother,
                    profession: e
                  }
                })
              } }
              value={ personalDetails.mother.profession }
              required={ true }
            />
          </div>
        </div>
      </div>
  )
}
export default PersonalDetails
