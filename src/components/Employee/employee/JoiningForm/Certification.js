import React, { useState, useEffect } from 'react'
import { certificate } from './form'
import {
  TextInput
} from './Controls/Controls'

const Certification = ({ getCertification, certificationData })=>{
  const [certificationArr, setCertificationArr] = useState([])

  useEffect(()=>{
    if(certificationData && certificationData.length>0){
      setCertificationArr(certificationData)
    }
  }, [certificationData])

  const addForm = ()=>{
    setCertificationArr([...certificationArr, []])
  }

  const reset = ()=>{
    setCertificationArr([])
    getCertification([])
  }

  const setData = (payload, formKey)=>{
    const temp = [...certificationArr]
    temp[formKey] = payload
    setCertificationArr(temp)
    getCertification(temp)
  }

  return(
    <div>
        <h6>
          Certification <span onClick={ addForm } className="btn-link">Add New</span>
          &nbsp;<span onClick={ reset } className="btn-link text-danger">Reset</span>
        </h6>
        {certificationArr && certificationArr.length>0?
          <table className="table table-bordered">
          <thead>
            <tr>
              <th>Name</th>
              <th>Board/Society</th>
              <th>Month/Year</th>
            </tr>
          </thead>
          <tbody>
            {certificationArr.map((e, i)=>(
              <Cert
                key={ i }
                formKey={ i }
                setData={ setData }
                form={ e && e.name? e: certificate }
              />
            ))}
          </tbody>
        </table>
        :null}
      </div>
  )
}

const Cert = ({ formKey, setData, form })=>{
  const [certification, setCertification] = useState(form)

  const setter = (key, value)=>{
    const payload = {
      ...certification,
      [key]: value
    }
    setCertification(payload)
    setData(payload, formKey)
  }

  return(
    <tr>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('name', e)
          } }
          value={ certification.name }
          required={ true }
        />
      </td>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('board_society', e)
          } }
          value={ certification.board_society }
          required={ true }
        />
      </td>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('month_yr', e)
          } }
          value={ certification.month_yr }
          required={ true }
        />
      </td>
    </tr>
  )
}

export default Certification
