import React, { useState, useEffect } from 'react'
import { relative } from './form'
import {
  TextInput
} from './Controls/Controls'

const Relatives = ({ getRelatives, relativesData })=>{
  const [relativesArr, setRelativesArr] = useState([])

  useEffect(()=>{
    if(relativesData && relativesData.length>0){
      setRelativesArr(relativesData)
    }
  }, [relativesData])

  const addForm = ()=>{
    setRelativesArr([...relativesArr, []])
  }

  const reset = ()=>{
    setRelativesArr([])
    getRelatives([])
  }

  const setData = (payload, formKey)=>{
    const temp = [...relativesArr]
    temp[formKey] = payload
    setRelativesArr(temp)
    getRelatives(temp)
  }

  return(
    <div>
        <h6>
          Relatives <span onClick={ addForm } className="btn-link">Add New</span>
           &nbsp;<span onClick={ reset } className="btn-link text-danger">Reset</span>
        </h6>
        {relativesArr.map((e, i)=>(
          <Relative
            key={ i }
            formKey={ i }
            setData={ setData }
            form={ e && e.name? e : relative }
          />
        ))}
      </div>
  )
}

const Relative = ({ formKey, setData, form })=>{
  const [relative, setRelative] = useState(form)

  const setter = (key, value)=>{
    const payload = {
      ...relative,
      [key]: value
    }
    setRelative(payload)
    setData(payload, formKey)
  }

  return(
    <div className="row">
      <div className="col-lg-4">
        <TextInput
          label="Name"
          type="text"
          changeFunc={ (e)=>{
            setter('name', e)
          } }
          value={ relative.name }
          required={ true }
        />
      </div>
      <div className="col-lg-4">
        <TextInput
          label="Title"
          type="text"
          changeFunc={ (e)=>{
            setter('title', e)
          } }
          value={ relative.title }
          required={ true }
        />
      </div>
      <div className="col-lg-4">
        <TextInput
          label="Department"
          type="text"
          changeFunc={ (e)=>{
            setter('dept', e)
          } }
          value={ relative.dept }
          required={ true }
        />
      </div>
    </div>
  )
}

export default Relatives
