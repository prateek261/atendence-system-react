import React, { useState, useEffect } from 'react'
import Datetime from 'react-datetime'

export const TextInput = ({ label, type, changeFunc, value, required })=>{
  const [val, setVal] = useState('')

  useEffect(()=>{
    if(value){
      setVal(value)
    }else {
      setVal('')
    }
  }, [value])

  return (
    <div className={label? "form-group" : ''}>
      {label?
        <label>{ label }</label>
      :null}
      <input type={ type }
        onChange={(e)=>{
          setVal(e.target.value)
          changeFunc(e.target.value)
        }}
        value={ val }
        required={ required || false }
        className="form-control"
      />
    </div>
  )
}

export const TextArea = ({ label, changeFunc, value, required })=>{
  return (
    <div className={label? "form-group" : ''}>
      {label?
        <label>{ label }</label>
      :null}
      <textarea
        onChange={(e)=>{
          changeFunc(e.target.value)
        }}
        value={ value }
        required={ required || false }
        className="form-control"
      ></textarea>
    </div>
  )
}

export const DateInput = ({ label, changeFunc, value, required, format })=>{

  return(
    <div className={label? "form-group" : ''}>
      {label?
        <label>{ label }</label>
      :null}
      <Datetime
        dateFormat={`${ format? format : "DD/MM/YY" }`}
        timeFormat={false}
        onChange={(e)=>{
          try {
            changeFunc(e)
          } catch (el) {
            changeFunc('')
          }
        }}
        value={ value }
        inputProps={{ required: required }}
      />
    </div>
  )
}
