export const form = {
  first_name: '',
  middle_name: '',
  last_name: '',
  date_of_joining: '',
  job_title: '',
  dob: '',
  code: '',
  email: '',
  phone: '',
  aadhar_card: '',
  pan_card: '',
  avail_sick: '',
  avail_casual: '',
  email_password: '',
  passport: {
    date_of_issue: '',
    place_of_issue: '',
    date_of_expiry: '',
  },
  employment_visa_denied: '',
  relatives: '',
  address: {
    current: {
      address: '',
      telephone_no: '',
    },
    permanent: {
      address: '',
      telephone_no: '',
    }
  },
  emergency_contact_inforation: {
    primary_contact: {
      name: '',
      relationship: '',
      city: '',
      address: '',
      phone: '',
    },
    secondary_contact: {
      name: '',
      relationship: '',
      city: '',
      address: '',
      phone: '',
    }
  },
  personal_details: {
    martial_status: '',
    spouse_name_and_profession: '',
    no_of_children: '',
    father: {
      name: '',
      profession: ''
    },
    mother: {
      name: '',
      profession: ''
    },
    driving_license: '',
  },
  emplyee_history: '',
  education: '',
  training: '',
  certification: '',
  references: {
    first: '',
    second: '',
  }
}

export const relative = {
  name: '',
  title: '',
  dept: ''
}

export const emplyee_history = {
  fromDate: '',
  toDate: '',
  orgnization: '',
  title_and_key_responbilities: '',
  reporting_to_position: '',
  salary_pm_and_Perks: '',
  reasons_for_leaving: ''
}

export const educationForm = {
  qualification: '',
  university_board: '',
  specialization: '',
  year_of_passing: '',
  Grade_CGPA: ''
}

export const certificate = {
  name: '',
  board_society: '',
  month_yr:''
}
//
// Digital Sign [upload file]
//
// Documents
// [
//   10th,
//   12th,
//   Bachelor's Degree,
//   Master's Degree,
//   IDProof,
//   Pan Card,
//   reliving letter,
//   Letter,
//   experence certificate,
//   Bank passbook
// ]
