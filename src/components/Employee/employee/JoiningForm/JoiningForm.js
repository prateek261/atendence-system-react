import React, { useState, useEffect } from 'react'
import { form } from './form'
import {
  TextInput,
  TextArea,
  DateInput
} from './Controls/Controls'

import Passport from './Passport'
import Relatives from './Relatives'
import Address from './Address'
import EmergencyContact from './EmergencyContact'
import PersonalDetails from './PersonalDetails'
import EmployeeHistory from './EmployeeHistory'
import Education from './Education'
import Training from './Training'
import Certification from './Certification'
import References from './References'

const JoiningForm = ()=>{
  const [joiningForm, setJoiningForm] = useState(form)

  useEffect(()=>{
    console.log(joiningForm)
  }, [joiningForm])

  const submitForm = (e)=>{
    e.preventDefault()
    console.log(joiningForm)
  }

  return(
    <div className="card bg-light">
      <div className="card-body">
        <form onSubmit={ submitForm } method="post">
        <div className="row">
          <div className="col-lg-4">
            <TextInput
              label="First Name"
              type="text"
              changeFunc={ (e)=>{
                setJoiningForm({
                  ...joiningForm,
                  first_name: e
                })
              } }
              value={ joiningForm.first_name }
              required={ true }
            />
          </div>
          <div className="col-lg-4">
            <TextInput
              label="Middle Name"
              type="text"
              changeFunc={ (e)=>{
                setJoiningForm({
                  ...joiningForm,
                  middle_name: e
                })
              } }
              value={ joiningForm.middle_name }
              required={ true }
            />
          </div>
          <div className="col-lg-4">
            <TextInput
              label="Last Name"
              type="text"
              changeFunc={ (e)=>{
                setJoiningForm({
                  ...joiningForm,
                  last_name: e
                })
              } }
              value={ joiningForm.last_name }
              required={ true }
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4">
            <DateInput
              label="Date of Joining"
              changeFunc={ (e)=>{
                setJoiningForm({
                  ...joiningForm,
                  date_of_joining: e
                })
              } }
              value={ joiningForm.date_of_joining }
              required={ true }
            />
          </div>
          <div className="col-lg-4">
            <TextInput
              label="Job Title"
              type="text"
              changeFunc={ (e)=>{
                setJoiningForm({
                  ...joiningForm,
                  job_title: e
                })
              } }
              value={ joiningForm.job_title }
              required={ true }
            />
          </div>
          <div className="col-lg-4">
            <DateInput
              label="Date of Birth"
              changeFunc={ (e)=>{
                setJoiningForm({
                  ...joiningForm,
                  dob: e
                })
              } }
              value={ joiningForm.dob }
              required={ true }
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4">
            <Passport
              getPassport={ (passport)=>{
                setJoiningForm({
                  ...joiningForm,
                  passport: passport
                })
              } }
            />
          </div>
          <div className="col-lg-8">
            <TextArea
              label="Have your Employment Visa denied in any countries if yes detail the reason[s]"
              changeFunc={ (e)=>{
                setJoiningForm({
                  ...joiningForm,
                  employment_visa_denied: e
                })
              } }
              value={ joiningForm.employment_visa_denied }
              required={ true }
            />
          </div>
        </div>
        <Relatives
          getRelatives={ (relatives)=>{
            setJoiningForm({
              ...joiningForm,
              relatives,
            })
          } }
        />
        <Address
          getAddress={ (address)=>{
            setJoiningForm({
              ...joiningForm,
              address,
            })
          } }
        />
        <EmergencyContact
          getContact={ (emergency_contact_inforation)=>{
            setJoiningForm({
              ...joiningForm,
              emergency_contact_inforation,
            })
          } }
        />
        <PersonalDetails
          getDetails={ (personal_details)=>{
            setJoiningForm({
              ...joiningForm,
              personal_details,
            })
          } }
        />
        <EmployeeHistory
          getHistory={ (emplyee_history)=>{
            setJoiningForm({
              ...joiningForm,
              emplyee_history,
            })
          } }
        />
        <Education
          getEducation={ (education)=>{
            setJoiningForm({
              ...joiningForm,
              education,
            })
          } }
        />
        <Training
          getTraining={ (training)=>{
            setJoiningForm({
              ...joiningForm,
              training,
            })
          }}
        />
        <Certification
          getCertification={ (certification)=>{
            setJoiningForm({
              ...joiningForm,
              certification,
            })
          } }
        />
        <References
          getReferences={ (references)=>{
            setJoiningForm({
              ...joiningForm,
              references,
            })
          } }
        />
      <input type="submit" value="Submit" className="btn btn-primary" />
        </form>
      </div>
    </div>
  )
}
export default JoiningForm
