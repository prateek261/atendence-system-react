import React, { useState, useEffect } from 'react'
import {
  TextInput,
  DateInput
} from './Controls/Controls'
import moment from 'moment'
import { form } from './form'

const Passport = ({ getPassport, passportData, date_of_expiry })=>{
  const [passport, setPassport] = useState(form.passport)

  useEffect(()=>{
    if(passportData && passportData.date_of_issue){
      const { date_of_issue, place_of_issue,  } = passportData
      setPassport({
        date_of_issue: moment(date_of_issue),
        place_of_issue,
        date_of_expiry: moment(date_of_expiry)
      })
    }
}, [passportData])

  const setter = (key, value)=>{
    const payload = {
      ...passport,
      [key]: value
    }
    setPassport(payload)
    getPassport(payload)
  }

  return(
    <div>
      <h6>Passport No</h6>
      <DateInput
        label="Date of Issue"
        changeFunc={ (e)=>{
          setter('date_of_issue', e)
        } }
        value={ passport.date_of_issue }
        required={ false }
      />
      <TextInput
        label="Place of Issue"
        type="text"
        changeFunc={ (e)=>{
          setter('place_of_issue', e)
        } }
        value={ passport.place_of_issue }
        required={ false }
      />
      <DateInput
        label="Date of Expiry"
        changeFunc={ (e)=>{
          setter('date_of_expiry', e)
        } }
        value={ passport.date_of_expiry }
        required={ false }
      />
    </div>
  )
}
export default Passport
