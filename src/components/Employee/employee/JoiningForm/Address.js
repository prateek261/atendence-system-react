import React, { useState, useEffect } from 'react'
import {
  TextInput
} from './Controls/Controls'
import { form } from './form'

const Address = ({ getAddress, addressData })=>{
  const [address, setAddress] = useState(form.address)

  useEffect(()=>{
    if(addressData && addressData.current && addressData.current.address){
      setAddress(addressData)
    }
  }, [addressData])

  useEffect(()=>{
    getAddress(address)
  }, [address])

  const sameAsCurrent = (e)=>{
    if(e.target.checked){
      setAddress({
        ...address,
        permanent:{
          ...address.current,
        }
      })
    }else {

      setAddress({
        ...address,
        permanent:{
          ...address.permanent,
          address: '',
          telephone_no: ''
        }
      })
    }
  }

  return(
    <div>
      <h6>Address</h6>
      <div className="row">
        <div className="col-lg-6">
          <h6>Current Home Address</h6>
          <TextInput
            label="Address"
            type="text"
            changeFunc={ (e)=>{
              setAddress({
                ...address,
                current:{
                  ...address.current,
                  address: e
                }
              })
            } }
            value={ address.current.address }
            required={ true }
          />
          <TextInput
            label="Telephone No."
            type="number"
            changeFunc={ (e)=>{
              setAddress({
                ...address,
                current:{
                  ...address.current,
                  telephone_no: e
                }
              })
            } }
            value={ address.current.telephone_no }
            required={ true }
          />
        </div>
        <div className="col-lg-6">
          <h6>Permanent Home Address ( Same as current home address <input type="checkbox" onChange={ sameAsCurrent } /> )</h6>
          <TextInput
            label="Address"
            type="text"
            changeFunc={ (e)=>{
              setAddress({
                ...address,
                permanent:{
                  ...address.permanent,
                  address: e
                }
              })
            } }
            value={ address.permanent.address }
            required={ true }
          />
          <TextInput
            label="Telephone No."
            type="number"
            changeFunc={ (e)=>{
              setAddress({
                ...address,
                permanent:{
                  ...address.permanent,
                  telephone_no: e
                }
              })
            } }
            value={ address.permanent.telephone_no }
            required={ true }
          />
        </div>
      </div>
    </div>
  )
}
export default Address
