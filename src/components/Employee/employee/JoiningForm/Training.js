import React, { useState, useEffect } from 'react'
import {
  TextInput
} from './Controls/Controls'

const Training = ({ getTraining, trainingData })=>{
  const [trainingArr, setTrainingArr] = useState([])

  useEffect(()=>{
    if(trainingData && trainingData.length>0){
      setTrainingArr(trainingData)
    }
  }, [trainingData])

  const addForm = ()=>{
  setTrainingArr([...trainingArr, []])
  }

  const reset = ()=>{
    setTrainingArr([])
    getTraining([])
  }

  const setData = (payload, formKey)=>{
    const temp = [...trainingArr]
    temp[formKey] = payload
    setTrainingArr(temp)
    getTraining(temp)
  }

  return(
    <div>
        <h6>
          Training <span onClick={ addForm } className="btn-link">Add New</span>
          &nbsp;<span onClick={ reset } className="btn-link text-danger">Reset</span>
        </h6>
        {trainingArr && trainingArr.length>0?
          <table className="table table-bordered">
          <thead>
            <tr>
              <th>Column 1</th>
              <th>Column 2</th>
              <th>Column 3</th>
            </tr>
          </thead>
          <tbody>
            {trainingArr.map((e, i)=>(
              <Form
                key={ i }
                formKey={ i }
                setData={ setData }
                form={ e && e.col1 ? e : { col1: '', col2: '', col3: ''} }
              />
            ))}
          </tbody>
        </table>
        :null}
      </div>
  )
}

const Form = ({ formKey, setData, form })=>{
  const [training, setTraining] = useState(form)

  const setter = (key, value)=>{
    const payload = {
      ...training,
      [key]: value
    }
    setTraining(payload)
    setData(payload, formKey)
  }

  return(
    <tr>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('col1', e)
          } }
          value={ form.col1 }
          required={ true }
        />
      </td>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('col2', e)
          } }
          value={ form.col2 }
          required={ true }
        />
      </td>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('col3', e)
          } }
          value={ form.col3 }
          required={ true }
        />
      </td>
    </tr>
  )
}

export default Training
