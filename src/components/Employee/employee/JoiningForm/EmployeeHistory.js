import React, { useState, useEffect } from 'react'
import { emplyee_history } from './form'
import {
  TextInput,
  DateInput
} from './Controls/Controls'
import moment from 'moment'

const EmployeeHistory = ({ getHistory, historyData })=>{
  const [historyArr, setHistoryArr] = useState([])

  useEffect(()=>{
    if(historyData && historyData.length>0){
      setHistoryArr(historyData)
    }
  }, [historyData])

  const addForm = ()=>{
    setHistoryArr([...historyArr, []])
  }

  const reset = ()=>{
    setHistoryArr([])
    getHistory([])
  }

  const setData = (payload, form_key)=>{
    const temp = [...historyArr]
    temp[form_key] = payload
    setHistoryArr(temp)
    getHistory(temp)
  }

  return(
    <div>
      <h6>
        Employee History <span onClick={ addForm } className="btn-link">Add New</span>
        &nbsp;<span onClick={ reset } className="btn-link text-danger">Reset</span>
      </h6>
      <p style={{margin:0}}>Please list your most recent employer first</p>
      {historyArr && historyArr.length>0?
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>From</th>
              <th>To</th>
              <th>Orgnization</th>
              <th>Title & Key Responbilities</th>
              <th>Reporting to [Position]</th>
              <th>Salary[PM] & Perks</th>
              <th>Reasons for Leaving</th>
            </tr>
          </thead>
          <tbody>
            {historyArr.map((e, i)=>(
              <History
                key={ i }
                form_key={ i }
                setData={ setData }
                form={ e && e.fromDate? e: '' }
              />
            ))}
          </tbody>
        </table>
      :null}
    </div>
  )
}

const History = ({ form_key, setData, form })=>{
  const [history, setHistory] = useState(emplyee_history)

  useEffect(()=>{
    if(form){
      setHistory({
        ...form,
        fromDate: moment(form.fromDate),
        toDate: moment(form.toDate),
      })
    }
  }, [form])

  const setter = (key, value)=>{
    const payload = {
      ...history,
      [key]: value
    }
    setHistory(payload)
    setData(payload, form_key)
  }

  return(
    <tr>
      <td>
        <DateInput
          changeFunc={ (e)=>{
            setter('fromDate', e)
          } }
          value={ history.fromDate }
          required={ true }
        />
      </td>
      <td>
        <DateInput
          changeFunc={ (e)=>{
            setter('toDate', e)
          } }
          value={ history.toDate }
          required={ true }
        />
      </td>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('orgnization', e)
          } }
          value={ history.orgnization }
          required={ true }
        />
      </td>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('title_and_key_responbilities', e)
          } }
          value={ history.title_and_key_responbilities }
          required={ true }
        />
      </td>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('reporting_to_position', e)
          } }
          value={ history.reporting_to_position }
          required={ true }
        />
      </td>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('salary_pm_and_Perks', e)
          } }
          value={ history.salary_pm_and_Perks }
          required={ true }
        />
      </td>
      <td>
        <TextInput
          type="text"
          changeFunc={ (e)=>{
            setter('reasons_for_leaving', e)
          } }
          value={ history.reasons_for_leaving }
          required={ true }
        />
      </td>
    </tr>
  )
}

export default EmployeeHistory
