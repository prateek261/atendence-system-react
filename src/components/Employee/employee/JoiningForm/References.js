import React, { useState, useEffect } from 'react'
import { form } from './form'
import {
  TextArea
} from './Controls/Controls'

const References = ({ getReferences, referenceData })=>{
  const [references, setReferences] = useState(form.references)

  useEffect(()=>{
    if(referenceData && referenceData.first){
      setReferences(referenceData)
    }
  }, [referenceData])

  useEffect(()=>{
    getReferences(references)
  }, [references])

  return(
    <div>
      <h6>References</h6>
      <p style={{margin:0}}>[Name two individuals who can provide professionals reference]</p>
      <div className="row">
        <div className="col-lg-6">
          <TextArea
            label="First"
            changeFunc={ (e)=>{
              setReferences({
                ...references,
                first: e
              })
            } }
            value={ references.first }
            required={ true }
          />
        </div>
        <div className="col-lg-6">
          <TextArea
            label="Second"
            changeFunc={ (e)=>{
              setReferences({
                ...references,
                second: e
              })
            } }
            value={ references.second }
            required={ false }
          />
        </div>

      </div>
    </div>
  )
}

export default References
