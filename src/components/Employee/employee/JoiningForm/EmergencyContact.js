import React, { useState, useEffect } from 'react'
import {
  TextInput
} from './Controls/Controls'
import { form } from './form'

const EmergencyContact = ({ getContact, infoData })=>{
  const [info, setInfo] = useState(form.emergency_contact_inforation)

  useEffect(()=>{
    if(infoData && infoData.primary_contact && infoData.primary_contact.name){
      setInfo(infoData)
    }
  }, [infoData])

  useEffect(()=>{
    getContact(info)
  }, [info])

  return(
    <div>
      <h6>Emergency Contact Information</h6>
        <div className="row">
          <div className="col-lg-6">
            <h6>Primary Contact</h6>
            <TextInput
              label="Name"
              type="text"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  primary_contact:{
                    ...info.primary_contact,
                    name: e
                  }
                })
              } }
              value={ info.primary_contact.name }
              required={ true }
            />
            <TextInput
              label="Relationship"
              type="text"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  primary_contact:{
                    ...info.primary_contact,
                    relationship: e
                  }
                })
              } }
              value={ info.primary_contact.relationship }
              required={ true }
            />
            <TextInput
              label="City"
              type="text"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  primary_contact:{
                    ...info.primary_contact,
                    city: e
                  }
                })
              } }
              value={ info.primary_contact.city }
              required={ true }
            />
            <TextInput
              label="Address"
              type="text"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  primary_contact:{
                    ...info.primary_contact,
                    address: e
                  }
                })
              } }
              value={ info.primary_contact.address }
              required={ true }
            />
            <TextInput
              label="Phone"
              type="number"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  primary_contact:{
                    ...info.primary_contact,
                    phone: e
                  }
                })
              } }
              value={ info.primary_contact.phone }
              required={ true }
            />
          </div>
          <div className="col-lg-6">
            <h6>Secondary Contact</h6>
            <TextInput
              label="Name"
              type="text"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  secondary_contact:{
                    ...info.secondary_contact,
                    name: e
                  }
                })
              } }
              value={ info.secondary_contact.name }
              required={ false }
            />
            <TextInput
              label="Relationship"
              type="text"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  secondary_contact:{
                    ...info.secondary_contact,
                    relationship: e
                  }
                })
              } }
              value={ info.secondary_contact.relationship }
              required={ false }
            />
            <TextInput
              label="City"
              type="text"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  secondary_contact:{
                    ...info.secondary_contact,
                    city: e
                  }
                })
              } }
              value={ info.secondary_contact.city }
              required={ false }
            />
            <TextInput
              label="Address"
              type="text"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  secondary_contact:{
                    ...info.secondary_contact,
                    address: e
                  }
                })
              } }
              value={ info.secondary_contact.address }
              required={ false }
            />
            <TextInput
              label="Phone"
              type="number"
              changeFunc={ (e)=>{
                setInfo({
                  ...info,
                  secondary_contact:{
                    ...info.secondary_contact,
                    phone: e
                  }
                })
              } }
              value={ info.secondary_contact.phone }
              required={ false }
            />
          </div>
        </div>
      </div>
  )
}
export default EmergencyContact
