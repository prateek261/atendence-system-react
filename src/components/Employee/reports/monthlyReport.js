import React, {useState, useEffect} from 'react';
import Header from '../subComponents/header';
import Path from '../subComponents/path';
import MainSidebar from '../subComponents/sidebar';
import DataTable from 'react-data-table-component';
import {NavLink} from 'react-router-dom';
import axios from 'axios';
import {serverUrl, tokenHeaders, getId} from '../../Admin/utils/ajaxHeaders';
import moment from 'moment';

const columns = [
  {
    name: 'Date',
    selector: 'daydate.date',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{moment(row.daydate.date, "YYYY-MM-DD").format("DD-MMM-YYYY")}</span>
  },
  {
    name: 'Day',
    selector: 'daydate.day',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{row.daydate.day === "Saturday" || row.daydate.day === "Sunday"? <span className="text-danger">{row.daydate.day}</span>: <span>{row.daydate.day}</span>}</span>
  },
  {
    name: 'Day Type',
    selector: 'daydate.day_type',
    sortable: true,
  },
  {
    name: 'In-Time',
    selector: 'in_time',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{row.in_time !== "00:00:00"? moment(row.in_time, "hh:mm:ss").format("hh:mm A"): '-'}</span>
  },
  {
    name: 'Out-Time',
    selector: 'out_time',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{row.out_time !== "00:00:00"? moment(row.out_time, "hh:mm:ss").format("hh:mm A"): '-'}</span>
  },
  {
    name: 'Hour Worked',
    selector: 'hour_worked',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{`${row.hour_worked !== "00:00:00"? `${moment(row.hour_worked, "hh:mm:ss").format("hh:mm")} Hrs.`: '-'} `}</span>
  },
  {
    name: 'Status',
    selector: 'status_type',
    sortable: true,
    cell: row=> <span  className={`badge ${checkStatus(row.status_type)}`} style={{fontSize: '13px', position: 'relative', zIndex: '999'}}>{row.status_type}</span>
  }
]

const badges = [
  "badge-success",
  "badge-warning",
  "badge-danger",
  "badge-primary"
]

const statusTypes = [
  ["PRESENT", "PAID LEAVE", "SICK LEAVE", "CASUAL HOLIDAY"],
  ["INFORMED", "ABS INFORMED", "HALF DAY", "LATE ARRIVAL", "MISS PUNCH", "NONE"],
  ["ABSENT"],
  ["WEEKEND", "PUBLIC HOLIDAY", "FESTIVAL HOLIDAY", "HOLIDAY", "HALF WORKING DAY"]
]

const checkStatus = (status)=>{
  try{
    let st = statusTypes.map((e, i)=>{
      if(e.includes(status)){
        return i
      }
    })
    st = st.filter((e, i)=>{
      return e !==undefined
    })
    return badges[st[0]]
  }catch(e){

  }
}

const generateYears = ()=>{
  let years = [];
  const date = parseInt(moment().format('YYYY'))
  for(let i=-5; i < 5; i++){
    years.push(date + i)
  }
  return years;
}
const generateMonths = ()=>{
  let months = [];
  for(let i=1; i < 13; i++){
    months.push(i)
  }
  return months;
}

const EmpMonthlyReport = ({match})=>{
  const path = [{name: 'Monthly Report', url: '/admin/monthly-report'}]
  const years = generateYears()
  const months = generateMonths()
  const [month, setMonth] = useState('')
  const [year, setYear] = useState('')
  const [report, setReport] = useState([])
  const [totalReport, setTotalReport] = useState([])
  const user_id = getId()

  useEffect(()=>{
    const date = moment()
    const m = date.format('MM')
    const y = date.format('YYYY')
    setMonth(m)
    setYear(y)
  }, [])

  useEffect(()=>{
    if(user_id, month, year){
      axios.get(`${serverUrl}attendance/employee-by-month/${user_id}/${month}/${year}/`, {headers: tokenHeaders})
      .then(res=>{
        setReport(res.data.month_static)
        setTotalReport(res.data.calender_activity)
      })
      .catch(err=>{
        console.log(err);
      })
    }
  }, [month, year])

  return(
    <article>
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <div>
                    <table>
                      <tbody>
                        <tr>
                          <td>
                            <label>Month</label>
                            <select className="form-control" onChange={(e)=> setMonth(e.target.value)} value={month}>
                                <option value="">Select</option>
                              {months && months.length>0?
                                months.map((e, i)=>{
                                  if(e<10){
                                    return(
                                      <option key={i} value={`0${e}`}>{moment(e, "MM").format("MMMM")}</option>
                                    )
                                  }else {
                                    return(
                                      <option key={i} value={e}>{moment(e, "MM").format("MMMM")}</option>
                                    )
                                  }
                                })
                              :null}
                            </select>
                          </td>
                          <td>
                            <label>Year</label>
                            <select className="form-control" onChange={(e)=> setYear(e.target.value)} value={year}>
                                <option value="">Select</option>
                              {years && years.length>0?
                                years.map((e, i)=>(
                                  <option key={i}>{e}</option>
                                ))
                              :null}
                            </select>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <hr/>
                  </div>
                  <ReportCard
                    title={`Monthly Employees Report:- ${moment(month+"-"+year, "MM-YYYY").format("MMMM YYYY")}`}
                    columns={columns}
                    report={report}
                    totalReport={totalReport}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>

  )
}

const ReportCard = ({ title, columns, report , totalReport})=>{
  const { WORKING, HOLIDAY, PUBLIC_HOLIDAY, WEEKEND, HALF_WORKING_DAY } = totalReport;
  return(
    <div className="card bg-light">
      <div className="card-header">{title}</div>
      <div className="card-body">
        <DataTable
          paginationRowsPerPageOptions={[10,25,50,100]}
          pagination={true}
          columns={columns}
          data={report}
        />
      </div>
      <div className="card-footer report-table-container">
        <table className="report-table" style={{width: "100%"}}>
          <thead>
            <tr>
              <th>WORKING</th>
              <th>HOLIDAY</th>
              <th>PUBLIC HOLIDAY</th>
              <th>WEEKEND</th>
              <th>HALF WORKING DAY</th>
            </tr>
          </thead>
          <tbody>
            <tr>
            <td>{WORKING}</td>
            <td>{HOLIDAY}</td>
            <td>{PUBLIC_HOLIDAY}</td>
            <td>{WEEKEND}</td>
            <td>{HALF_WORKING_DAY}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default EmpMonthlyReport;
