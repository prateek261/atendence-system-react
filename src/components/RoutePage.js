import React, {useState} from 'react';
import {NavLink} from 'react-router-dom';
import logo from '../assets/img/logo.png';

const RoutePage = ({match})=>{

  return(
      <article>
          <section className="sectionColumns">
            <div className="leftColumn">
                <div className="content-leftColumn">
                  <div className="columnImg" style={{padding: '20%'}}>
                    <img src={logo} alt="sideimg"/>
                  </div>
                    <div className="container">

                    </div>
                </div>
            </div>
              <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4 loginHeader">Login</h5>
                      <div className="boxComman">
                        <div className="form-group">
                          <NavLink className="btn btnlg" to="/employee">Employee Login</NavLink>
                          <br/><br/>
                          <NavLink className="btn btnlg" to="/admin">Admin Login</NavLink>
                        </div>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
}

export default RoutePage;
