import React, {useState, useEffect} from 'react';
import {NavLink} from 'react-router-dom';
import {Redirect } from 'react-router';
import head_logo from '../../../assets/img/head.png';
import axios from 'axios';
import {serverUrl, headers} from '../utils/ajaxHeaders';

const Header = ({props})=>{
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [image, setImage] = useState('')
  const [redirect, setRedirect] = useState(false)

  useEffect(()=>{
    let token = localStorage.getItem('token');
    try{
      axios.post(`${serverUrl}account-sign/check-token/`, {token: token, user_type: 'is_admin'}, {headers: headers})
      .then(res=>{
        if(res.data.status){

        }else {
          remStorage()
        }
      })
      .catch(err=>{
        console.log(err);
        remStorage()
      })
    }catch(e){
      remStorage()
    }
  }, [])

  const remStorage = ()=>{
    localStorage.removeItem('user')
    localStorage.removeItem('token')
    setRedirect(true)
  }

  if(redirect){
      return(
        <Redirect to="/admin" />
      )
    }
  return(
      <div className="dasboardHeader">
          <div className="topbar">
              <div className="container-fluid">
                  <div className="row align-items-center">
                      <div className="col-12 col-sm-3 col-md-6">
                          <h1 id="logo">
                              <a href="#a">
                                <img alt="" style={{width: "150px"}} src={head_logo} className="img-fluid logo"/>
                              </a>
                          </h1>
                      </div>
                      <div className="col-12 col-sm-9 col-md-6 text-right">
                          <ul className="listTopMenu">
                              <li>
                                  <NavLink to="/admin/logout" className="dropdown-item">Logout</NavLink>
                              </li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    )
}

export default Header;
