import React from 'react';
import {NavLink} from 'react-router-dom';

const Path = ({path})=>{
  return(
    <nav aria-label="breadcrumb">
        <ol className="breadcrumb mb-0">
          {path && path.length>0?
            path.map((e, i)=>(
              <li key={i} className="breadcrumb-item">
                <NavLink to={e.url}>{e.name}</NavLink>
              </li>
            ))
          :null}

        </ol>
    </nav>
  )
}

export default Path;
