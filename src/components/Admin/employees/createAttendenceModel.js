import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Datetime from 'react-datetime';
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders';
import '../../../assets/css/fixed.css';

const initial = {
  profile: '',
  date: '',
  daydate: '',
  in_time: '',
  out_time: '',
  status_type: 'PRESENT'
}

const CreateAttendenceModel = ({id, close})=>{
  const [attendance, setAttendance] = useState(initial)
  const [statusList, setStatusList] = useState([])

  useEffect(()=>{
    setAttendance({
      ...attendance,
      profile: parseInt(id)
    })
  }, [])

  useEffect(()=>{
    axios.get(`${serverUrl}configure/choice/STATUS_TYPE/`, {headers: tokenHeaders})
    .then(res=>{
      createStatusList(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  const createAttendence = (e)=>{
    e.preventDefault()
    axios.post(`${serverUrl}attendance/data/`, attendance, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        alert(res.data.message)
        close()
      }else {
        alert(res.data.message)
      }
    })
    .catch(err=>{
      console.log(err);
      alert(err)
    })
  }
  const createStatusList = (list)=>{
    let status = []
    list.map((e, i)=>{
      if(e && e.length>0){
        if(Array.isArray(e[1])){
          e[1].map((f, j)=>{
            status.push(f[0])
          })
        }else {
          status.push(e[0])
        }
      }
    })
    setStatusList(status);
  }
  const checkDate = (e)=>{
    const payload = {
      date: e
    }
    axios.post(`${serverUrl}daydate/check-date/`, payload, {headers: tokenHeaders})
    .then(res=>{

      if(res.data && res.data.length>0){
        setAttendance({
          ...attendance,
          daydate: res.data[0].id,
          date: e
        })
      }else {
        alert("Activity does not exist, please create activity.")
        close()
      }
    })
    .catch(err=>{
      console.log(err);
      alert(err)
    })
  }
  return(
    <div className="fixed card">
      <form method="POST" onSubmit={createAttendence}>
        <div className="card-header">Create Attendance
          <span className="close" onClick={()=>close()}>&times;</span>
        </div>
        <div className="card-body">
          <div className="form-group">
            <label>Choose Date</label>
            <Datetime
              dateFormat="YYYY-MM-DD"
              timeFormat={false}
              utc={true}
              onChange={(e)=>{
                try {
                  setAttendance({...attendance, date:e.format("YYYY-MM-DD")})
                  checkDate(e.format("YYYY-MM-DD"))
                } catch (e) {

                }
              }} value={attendance.date}
              required
            />
          </div>
          {attendance.daydate?
            <React.Fragment>
              <div className="form-group">
                <label>In-Time (in 24 Hours)</label>
                <Datetime
                  dateFormat={false}
                  timeFormat="HH:mm:ss"
                  utc={true}
                  onChange={(e)=>{
                    try {
                      setAttendance({...attendance, in_time:e.format("HH:mm:ss")})
                    } catch (e) {

                    }
                  }} value={attendance.in_time}
                  required
                />
              </div>
              <div className="form-group">
                <label>Out-Time (in 24 Hours)</label>
                <Datetime
                  dateFormat={false}
                  timeFormat="HH:mm:ss"
                  utc={true}
                  onChange={(e)=>{
                    try {
                      setAttendance({...attendance, out_time:e.format("HH:mm:ss")})
                    } catch (e) {

                    }
                  }} value={attendance.out_time}
                  required
                />
              </div>
              <div className="form-group">
                <label>Status</label>
                <select className="form-control" required onChange={(e)=> setAttendance({...attendance, status_type:e.target.value})} value={attendance.status_type}>
                  <option value="">Select Status</option>
                  {statusList.map((e, i)=>(
                    <option key={i}>{e}</option>
                  ))}
                </select>
              </div>
              <div className="form-group">
                <input type="submit" value="Update" className="btn" />
              </div>
            </React.Fragment>
          :null}
        </div>
      </form>
    </div>
  )
}
export default CreateAttendenceModel;
