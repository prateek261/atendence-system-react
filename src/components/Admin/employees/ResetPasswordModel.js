import React, { useState } from 'react'
import axios from 'axios'
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders'
import '../../../assets/css/fixed.css'

const ResetPasswordModel = ({ email, close })=>{
  const [password, setPassword] = useState('')

  const resetPassword = (e)=>{
    e.preventDefault()
    const payload = {
      email,
      new_password: password
    }
    axios.post(`${ serverUrl }account-sign/admin-reset-emp-pass/`, payload, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.message){
        setPassword('')
        close()
        alert(res.data.message)
      }
    })
    .catch(err=>{
      console.log(err)
      alert(err)
    })
  }

  return(
    <div className="fixed card">
      <form method="POST" onSubmit={ resetPassword }>
        <div className="card-header">Reset Password
          <span className="close" onClick={ ()=>close() }>&times;</span>
        </div>
        <div className="card-body">
          <div className="form-group">
            <label>Password</label>
            <input type="password" required className="form-control" value={ password } onChange={ (e)=> setPassword(e.target.value) } />
          </div>
          <div className="form-group">
            <input type="submit" value="Update" className="btn" />
          </div>
        </div>
      </form>
    </div>
  )
}
export default ResetPasswordModel
