import React, {useState} from 'react';
import axios from 'axios';
import Datetime from 'react-datetime';
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders';
import '../../../assets/css/fixed.css';

const initial = {
  code: '',
  first_name: '',
  middle_name: '',
  last_name: '',
  email: '',
  avail_sick: '12',
  avail_paid: '10',
  created_DT: '',
  password: ''
}

const CreateEmployeeModel = ({match, close})=>{
  const path = [{name: 'Employees', url: '/employees'},
                {name: 'Create', url: "/employees/create"}]
  const [employee, setEmployee] = useState(initial)

  const createEmployee = (e)=>{
    e.preventDefault()
    axios.post(`${serverUrl}profile/data/`, employee, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        setEmployee(initial)
        alert(res.data.message)
        close()
      }else {
        alert(res.data.message)
      }
    })
    .catch(err=>{
      console.log(err);
      alert(err)
    })
  }
  return(
    <div className="fixed card">
      <form method="POST" onSubmit={createEmployee}>
        <div className="card-header">Create New Employee
          <span className="close" onClick={()=>close()}>&times;</span>
        </div>
        <div className="card-body modelScroll">
          <div className="form-group">
            <label>First Name</label>
            <input type="text" className="form-control" required onChange={(e)=> setEmployee({...employee, first_name: e.target.value})} value={employee.first_name} />
          </div>
          <div className="form-group">
            <label>Middle Name</label>
            <input type="text" className="form-control" onChange={(e)=> setEmployee({...employee, middle_name: e.target.value})} value={employee.middle_name} />
          </div>
          <div className="form-group">
            <label>Last Name</label>
            <input type="text" className="form-control" required onChange={(e)=> setEmployee({...employee, last_name: e.target.value})} value={employee.last_name} />
          </div>
          <div className="form-group">
            <label>Email</label>
            <input type="email" className="form-control" required onChange={(e)=> setEmployee({...employee, email:e.target.value})} value={employee.email} />
          </div>
          <div className="form-group">
            <label>Code</label>
            <input type="text" className="form-control" required onChange={(e)=> setEmployee({...employee, code:e.target.value})} value={employee.code} />
          </div>
          <div className="form-group">
            <label>Available Sick Leaves</label>
            <input type="text" className="form-control" required onChange={(e)=> setEmployee({...employee, avail_sick:e.target.value})} value={employee.avail_sick} />
          </div>
          <div className="form-group">
            <label>Available Paid Leaves</label>
            <input type="text" className="form-control" required onChange={(e)=> setEmployee({...employee, avail_paid:e.target.value})} value={employee.avail_paid} />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input type="password" className="form-control" required onChange={(e)=> setEmployee({...employee, password:e.target.value})} value={employee.password} />
          </div>
          <div className="form-group">
            <input type="submit" value="Create Employee" className="btn" />
            <button style={{marginLeft: '15px'}} onClick={()=>close()} className="btn">Close</button>
          </div>
        </div>
      </form>
    </div>
  )
}
export default CreateEmployeeModel;
