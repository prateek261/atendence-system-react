import React, { useState, useEffect } from 'react'
import Header from '../subComponents/header'
import Path from '../subComponents/path'
import MainSidebar from '../subComponents/sidebar'
import Datetime from 'react-datetime'
import axios from 'axios'
import {serverUrl, tokenHeaders} from '../../Admin/utils/ajaxHeaders'
import moment from 'moment'

import { form } from '../../Employee/employee/JoiningForm/form'
import {
  TextInput,
  TextArea,
  DateInput
} from '../../Employee/employee/JoiningForm/Controls/Controls'

import Passport from '../../Employee/employee/JoiningForm/Passport'
import Relatives from '../../Employee/employee/JoiningForm/Relatives'
import Address from '../../Employee/employee/JoiningForm/Address'
import EmergencyContact from '../../Employee/employee/JoiningForm/EmergencyContact'
import PersonalDetails from '../../Employee/employee/JoiningForm/PersonalDetails'
import EmployeeHistory from '../../Employee/employee/JoiningForm/EmployeeHistory'
import Education from '../../Employee/employee/JoiningForm/Education'
import Training from '../../Employee/employee/JoiningForm/Training'
import Certification from '../../Employee/employee/JoiningForm/Certification'
import References from '../../Employee/employee/JoiningForm/References'

const initial = {
  code: '',
  name: '',
  dob: '',
  email: '',
  phone: '',
  aadhar_card: '',
  pan_card: '',
  avail_sick: '',
  avail_casual: '',
}

const getProfile = ()=>{
  try {
    let user = localStorage.getItem('user')
    user = JSON.parse(user)
    return user.data
  } catch (e) {
    return null
  }
}

const EditDetailsAdmin = ({match})=>{
  const path = [{name: 'Employee Data Form', url: '/employee/details/edit'}]
  const [employee, setEmployee] = useState(getProfile() || initial)
  const [joiningForm, setJoiningForm] = useState(form)

  const [accept, setAccept] = useState(false)

  useEffect(()=>{
    axios.get(`${serverUrl}profile/data/${match.params.id}/`, {headers: tokenHeaders})
    .then(res=>{
      setEmployee(res.data[0])
      console.log(res.data[0]);
      const payload = {
        first_name: joiningForm.first_name,
        middle_name: joiningForm.middle_name,
        last_name: joiningForm.last_name,
        date_of_joining: moment(joiningForm.date_of_joining),
        job_title: joiningForm.job_title,
        dob: moment(joiningForm.dob),
        passport: JSON.parse(joiningForm.passport),
        employment_visa_denied: JSON.parse(joiningForm.employment_visa_denied),
        relatives: JSON.parse(joiningForm.relatives),
        address: JSON.parse(joiningForm.address),
        emergency_contact_inforation: JSON.parse(joiningForm.emergency_contact_inforation),
        personal_details: JSON.parse(joiningForm.paspersonal_detailssport),
        emplyee_history: JSON.parse(joiningForm.emplyee_history),
        education: JSON.parse(joiningForm.education),
        training: JSON.parse(joiningForm.training),
        certification: JSON.parse(joiningForm.certification),
        references: JSON.parse(joiningForm.references),
      }
      setJoiningForm(payload)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [match.params.id])

  const updateEmployee = (e)=>{
    e.preventDefault()
    const payload = {
      first_name: joiningForm.first_name,
      middle_name: joiningForm.middle_name,
      last_name: joiningForm.last_name,
      date_of_joining: joiningForm.date_of_joining.format("YYYY-MM-DD"),
      job_title: joiningForm.job_title,
      dob: joiningForm.dob.format("YYYY-MM-DD"),
      passport: '"'+JSON.stringify(joiningForm.passport)+'"',
      employment_visa_denied: '"'+JSON.stringify(joiningForm.employment_visa_denied)+'"',
      relatives: '"'+JSON.stringify(joiningForm.relatives)+'"',
      address: '"'+JSON.stringify(joiningForm.address)+'"',
      emergency_contact_inforation: '"'+JSON.stringify(joiningForm.emergency_contact_inforation)+'"',
      personal_details: '"'+JSON.stringify(joiningForm.paspersonal_detailssport)+'"',
      emplyee_history: '"'+JSON.stringify(joiningForm.emplyee_history)+'"',
      education: '"'+JSON.stringify(joiningForm.education)+'"',
      training: '"'+JSON.stringify(joiningForm.training)+'"',
      certification: '"'+JSON.stringify(joiningForm.certification)+'"',
      references: '"'+JSON.stringify(joiningForm.references)+'"',
    }


    axios.put(`${serverUrl}profile/update-data/${employee.id}/`, payload, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        alert(res.data.message)
      }else {
        alert(res.data.message)
      }
    })
    .catch(err=>{
      console.log(err);
      alert(err)
    })
  }

  return(
    <article>
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <form method="POST" onSubmit={ updateEmployee }>
                    <div className="row">
                      <div className="col-lg-4">
                        <TextInput
                          label="First Name"
                          type="text"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              first_name: e
                            })
                          } }
                          value={ joiningForm.first_name }
                          required={ true }
                        />
                      </div>
                      <div className="col-lg-4">
                        <TextInput
                          label="Middle Name"
                          type="text"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              middle_name: e
                            })
                          } }
                          value={ joiningForm.middle_name }
                          required={ false }
                        />
                      </div>
                      <div className="col-lg-4">
                        <TextInput
                          label="Last Name"
                          type="text"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              last_name: e
                            })
                          } }
                          value={ joiningForm.last_name }
                          required={ true }
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-4">
                        <DateInput
                          format="DD/MM/YY"
                          label="Date of Joining"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              date_of_joining: e
                            })
                          } }
                          value={ joiningForm.date_of_joining }
                          required={ true }
                        />
                      </div>
                      <div className="col-lg-4">
                        <TextInput
                          label="Job Title"
                          type="text"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              job_title: e
                            })
                          } }
                          value={ joiningForm.job_title }
                          required={ true }
                        />
                      </div>
                      <div className="col-lg-4">
                        <DateInput
                          format="DD/MM/YY"
                          label="Date of Birth"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              dob: e
                            })
                          } }
                          value={ joiningForm.dob }
                          required={ true }
                        />
                      </div>
                    </div>
                    <hr/>
                    <div className="row">
                      <div className="col-lg-4">
                        <Passport
                          getPassport={ (passport)=>{
                            setJoiningForm({
                              ...joiningForm,
                              passport: passport
                            })
                          } }
                        />
                      </div>
                      <div className="col-lg-8">
                        <TextArea
                          label="Have your Employment Visa denied in any countries if yes detail the reason[s]"
                          changeFunc={ (e)=>{
                            setJoiningForm({
                              ...joiningForm,
                              employment_visa_denied: e
                            })
                          } }
                          value={ joiningForm.employment_visa_denied }
                          required={ false }
                        />
                        <div className="row">
                          <div className="col-md-6">
                            <TextInput
                              label="Driving License"
                              type="text"
                              changeFunc={ (e)=>{
                                setJoiningForm({
                                  ...joiningForm,
                                  driving_license: e
                                })
                              } }
                              value={ joiningForm.driving_license }
                              required={ false }
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr/>
                    <Relatives
                      getRelatives={ (relatives)=>{
                        setJoiningForm({
                          ...joiningForm,
                          relatives,
                        })
                      } }
                    />
                    <hr/>
                    <Address
                      getAddress={ (address)=>{
                        setJoiningForm({
                          ...joiningForm,
                          address,
                        })
                      } }
                    />
                    <hr/>
                    <EmergencyContact
                      getContact={ (emergency_contact_inforation)=>{
                        setJoiningForm({
                          ...joiningForm,
                          emergency_contact_inforation,
                        })
                      } }
                    />
                    <hr/>
                    <PersonalDetails
                      getDetails={ (personal_details)=>{
                        setJoiningForm({
                          ...joiningForm,
                          personal_details,
                        })
                      } }
                    />
                    <hr/>
                    <EmployeeHistory
                      getHistory={ (emplyee_history)=>{
                        setJoiningForm({
                          ...joiningForm,
                          emplyee_history,
                        })
                      } }
                    />
                    <hr/>
                    <Education
                      getEducation={ (education)=>{
                        setJoiningForm({
                          ...joiningForm,
                          education,
                        })
                      } }
                    />
                    <hr/>
                    <Training
                      getTraining={ (training)=>{
                        setJoiningForm({
                          ...joiningForm,
                          training,
                        })
                      }}
                    />
                    <hr/>
                    <Certification
                      getCertification={ (certification)=>{
                        setJoiningForm({
                          ...joiningForm,
                          certification,
                        })
                      } }
                    />
                    <hr/>
                    <References
                      getReferences={ (references)=>{
                        setJoiningForm({
                          ...joiningForm,
                          references,
                        })
                      } }
                    />
                    <p>
                    <input type="checkbox" onChange={ (e)=>{
                      if(e.target.checked){
                        setAccept(true)
                      }else {
                        setAccept(false)
                      }
                    } } value={ accept } />
                    &nbsp;The above details are true and best of my knowledage. I understand that any misrepresentation of facts may be called for disciplinary action.</p>
                    <div className="form-group">
                      <input type="submit" disabled={ !accept } value="Submit" className="btn" />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>

  )
}
export default EditDetailsAdmin
