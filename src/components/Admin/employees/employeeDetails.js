import React, {useState, useEffect} from 'react';
import Header from '../subComponents/header';
import Path from '../subComponents/path';
import MainSidebar from '../subComponents/sidebar';
import Datetime from 'react-datetime';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders';
import { NavLink } from 'react-router-dom'

import DocumentModel from './DocumentModel'

const initial = {
  code: '',
  first_name: '',
  middle_name: '',
  last_name: '',
  dob: '',
  email: '',
  phone: '',
  aadhar_card: '',
  pan_card: '',
  avail_sick: '',
  avail_casual: '',
}

const EmployeeDetails = ({match})=>{
  const path = [{name: 'Employees', url: '/admin/employees'},
                {name: 'Details', url: `/admin/employees/${match.params.id}`}]
  const [employee, setEmployee] = useState(initial)

  const [docModel, setDocModel] = useState(false)

  useEffect(()=>{
    axios.get(`${serverUrl}profile/data/${match.params.id}/`, {headers: tokenHeaders})
    .then(res=>{
      setEmployee(res.data[0])
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  const updateEmployee = (e)=>{
    e.preventDefault()
    axios.put(`${serverUrl}profile/update-data/${match.params.id}/`, employee, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        alert(res.data.message)
      }else {
        alert(res.data.message)
      }
    })
    .catch(err=>{
      console.log(err);
      alert(err)
    })
  }
  return(
    <article>
      {docModel?
        <DocumentModel
          id={ match.params.id }
          close={ ()=> setDocModel(false) }
        />
      :null}
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <form method="POST" onSubmit={updateEmployee}>
                    <div className="row">
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>First Name</label>
                          <input type="text" required className="form-control" onChange={(e)=> setEmployee({...employee, first_name: e.target.value})} value={employee.first_name} />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Middle Name</label>
                          <input type="text" className="form-control" onChange={(e)=> setEmployee({...employee, middle_name: e.target.value})} value={employee.middle_name || ''} />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Last Name</label>
                          <input type="text" required className="form-control" onChange={(e)=> setEmployee({...employee, last_name: e.target.value})} value={employee.last_name} />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Date Of Birth</label>
                          <Datetime
                            dateFormat="YYYY-MM-DD"
                            timeFormat={false}
                            utc={true}
                            onChange={(e)=>{
                              setEmployee({...employee, dob:e.format("YYYY-MM-DD")})
                            }} value={employee.dob}
                            required={true}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Email</label>
                          <input type="email" required className="form-control" onChange={(e)=> setEmployee({...employee, email:e.target.value})} value={employee.email} />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Phone No.</label>
                          <input type="number" required className="form-control" onChange={(e)=> setEmployee({...employee, phone:e.target.value})} value={employee.phone} />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Employee Code</label>
                          <input type="text" required className="form-control" onChange={(e)=> setEmployee({...employee, code:e.target.value})} value={employee.code} />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Aadhar Card</label>
                          <input type="text" className="form-control" onChange={(e)=> setEmployee({...employee, aadhar_card:e.target.value})} value={employee.aadhar_card} />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Pan Card</label>
                          <input type="text" className="form-control" onChange={(e)=> setEmployee({...employee, pan_card:e.target.value})} value={employee.pan_card} />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Available Sick Leaves</label>
                          <input type="text" className="form-control" onChange={(e)=> setEmployee({...employee, avail_sick:e.target.value})} value={employee.avail_sick} />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4">
                        <div className="form-group">
                          <label>Available Paid Leaves</label>
                          <input type="text" className="form-control" onChange={(e)=> setEmployee({...employee, avail_casual:e.target.value})} value={employee.avail_casual} />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <input type="submit" value="Save Changes" className="btn" />
                      <NavLink to={`/admin/employees/${ match.params.id }/edit`} style={{marginLeft: '15px'}}>Edit Full Profile</NavLink>
                      <a href="#a" style={{marginLeft: '15px'}} onClick={ ()=> setDocModel(true) }>Upload Document</a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>

  )
}
export default EmployeeDetails;
