import React, {useState} from 'react';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders';
import '../../../assets/css/fixed.css';

const UploadAttendenceModel = ({close})=>{
  const [file, setFile] = useState('')

  const uploadExcel = (e) =>{
    e.preventDefault()
    let formdata = new FormData()
    formdata.append("attend_file", file[0])
    axios.post(`${serverUrl}attendance/upload-attendance-data/`, formdata, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.message){
        alert(res.data.message)
        close()
      }
    })
    .catch(err=>{
      console.log(err);
    })
  }
  return(
    <div className="fixed card">
      <form method="POST" onSubmit={uploadExcel}>
        <div className="card-header">Upload Attendence
          <span className="close" onClick={()=>close()}>&times;</span>
        </div>
        <div className="card-body">
          <div className="form-group">
            <label>Select Excel</label>
            <input type="file" required onChange={(e)=>{
              setFile(e.target.files)
            }} className="form-control" />
          </div>
          <div className="form-group">
            <input type="submit" value="Upload" className="btn" />
          </div>
        </div>
      </form>
    </div>
  )
}
export default UploadAttendenceModel;
