import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Datetime from 'react-datetime';
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders';
import '../../../assets/css/fixed.css';

const initial = {
  // in_time: '',
  // out_time: '',
  status_type: ''
}

const EditAttendenceModel = ({attendance, close})=>{
  const [data, setData] = useState(initial)
  const [statusList, setStatusList] = useState([])

  useEffect(()=>{
    setData({
      // in_time: attendance.in_time,
      // out_time: attendance.out_time,
      status_type: attendance.status_type
    })
  }, [])

  useEffect(()=>{
    axios.get(`${serverUrl}configure/choice/STATUS_TYPE/`, {headers: tokenHeaders})
    .then(res=>{
      createStatusList(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  const updateAttendence = (e)=>{
    e.preventDefault()
    axios.put(`${serverUrl}attendance/update-attendance-data/${attendance.id}/`, data, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        setData(initial)
        alert(res.data.message)
        close()
      }else {
        alert(res.data.message)
      }
    })
    .catch(err=>{
      console.log(err);
      alert(err)
    })
  }
  const createStatusList = (list)=>{
    let status = []
    list.map((e, i)=>{
      if(e && e.length>0){
        if(Array.isArray(e[1])){
          e[1].map((f, j)=>{
            status.push(f[0])
          })
        }else {
          status.push(e[0])
        }
      }
    })
    setStatusList(status);
  }
  return(
    <div className="fixed card">
      <form method="POST" onSubmit={updateAttendence}>
        <div className="card-header">Edit Attendance
          <span className="close" onClick={()=>close()}>&times;</span>
        </div>
        <div className="card-body">
          <div className="form-group">
            <label>Status</label>
            <select className="form-control" required onChange={(e)=> setData({...data, status_type:e.target.value})} value={data.status_type}>
              <option value="">Select Status</option>
              {statusList.map((e, i)=>(
                <option key={ i }>{e}</option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <input type="submit" value="Update" className="btn" />
          </div>
        </div>
      </form>
    </div>
  )
}
export default EditAttendenceModel;

/*  <div className="form-group">
    <label>In-Time</label>
    <Datetime
      dateFormat={false}
      timeFormat="HH:mm:ss"
      utc={true}
      onChange={(e)=>{
        try {
          setData({...data, in_time:e.format("HH:mm:ss")})
        } catch (e) {

        }
      }} value={data.in_time}
      required
    />
  </div>
  <div className="form-group">
    <label>Out-Time</label>
    <Datetime
      dateFormat={false}
      timeFormat="HH:mm:ss"
      utc={true}
      onChange={(e)=>{
        try {
          setData({...data, out_time:e.format("HH:mm:ss")})
        } catch (e) {

        }
      }} value={data.out_time}
      required
    />
  </div>*/
