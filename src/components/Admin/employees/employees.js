import React, {useState, useEffect} from 'react';
import Header from '../subComponents/header';
import Path from '../subComponents/path';
import MainSidebar from '../subComponents/sidebar';
import DataTable from 'react-data-table-component';
import {NavLink} from 'react-router-dom';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders';

import CreateEmployeeModel from './createEmployeeModel';
import UploadAttendenceModel from './uploadAttendenceModel';
import ResetPasswordModel from './ResetPasswordModel'

const Employees = ({match})=>{
  const path = [{name: 'Employees', url: '/admin/employees'}]
  const [employees, setEmployees] = useState([])
  const [model, setModel] = useState(false)
  const [uploadModel, setUploadModel] = useState(false)
  const [resetModel, setResetModel] = useState(false)
  const [empId, setEmpId] = useState('')

  const columns = [
    {
      name: 'Name',
      selector: 'name',
      sortable: true,
      cell: row=> <NavLink to={`/admin/employees/${row.id}/${row.first_name} ${row.middle_name? row.middle_name : ''} ${row.last_name? row.last_name : ''}/static`} style={{position: 'relative', zIndex: '999'}}>{row.first_name? row.first_name.toUpperCase() : ''} {row.middle_name? row.middle_name.toUpperCase() : ''} {row.last_name? row.last_name.toUpperCase() : ''}</NavLink>
    },
    {
      name: 'Code',
      selector: 'code',
      sortable: true,
    },
    {
      name: 'Email',
      selector: 'email',
      sortable: true,
    },
    {
      name: 'Phone No.',
      selector: 'phone',
      sortable: true,
    },
    {
      name: 'Available Sick',
      selector: 'avail_sick',
      sortable: true,
    },
    {
      name: 'Available Paid',
      selector: 'avail_casual',
      sortable: true,
    },
    {
      name: 'Active',
      selector: 'is_active',
      sortable: true,
      cell: row=> <span className={`badge ${row.is_active? 'badge-success': 'badge-danger'}`} style={{fontSize: '13px', cursor: 'pointer', position: 'relative', zIndex: '999'}}>{row.is_active? 'Active': 'Inactive'}</span>
    },
    {
      name: 'Action',
      selector: 'name',
      sortable: true,
      cell: row=> <span style={{position: 'relative', zIndex: '999'}}>
          <NavLink to={`/admin/employees/${row.id}`}>Edit</NavLink>
          <span className="btn-link" onClick={ ()=>{
            setEmpId(row.email)
            setResetModel(true)
          }} style={{marginLeft: '15px'}}>Reset Password</span>
        </span>
    },
  ]

  useEffect(()=>{
    axios.get(`${serverUrl}profile/data/`, {headers: tokenHeaders})
    .then(res=>{
      setEmployees(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [model])

  return(
    <article>
      {model?
        <CreateEmployeeModel close={()=> setModel(false) } />
      :null}
      {uploadModel?
        <UploadAttendenceModel close={()=> setUploadModel(false)} />
      :null}
      {resetModel?
        <ResetPasswordModel
          email={ empId }
          close={ ()=> setResetModel(false) }
        />
      :null}
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <div>
                    <span style={{color: '#0056b3', cursor: 'pointer'}} onClick={()=> setModel(true)}>Create New Employee</span>
                    <span style={{color: '#0056b3', cursor: 'pointer', marginLeft: '15px'}} onClick={()=> setUploadModel(true)}>Upload Attendence</span>
                    <hr/>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 col-md-12">
                      <DataTable
                        paginationRowsPerPageOptions={[10,25,50,100]}
                        pagination={true}
                        columns={columns}
                        data={employees}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>

  )
}
export default Employees;
