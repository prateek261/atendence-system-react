import React, { useState, useEffect, useRef } from 'react'
import axios from 'axios'
import { downloadUrl, serverUrl, tokenHeaders } from '../utils/ajaxHeaders'
import '../../../assets/css/fixed.css'

const DocumentModel = ({ id, close })=>{
  const [type, setType] = useState('')
  const [file, setFile] = useState('')
  const fileTypes = [
    "10th",
    "12th",
    "Bachelor's Degree",
    "Driving License/ or other id proofs",
    "PAN Card",
    "Reliving letter",
    "Experience certificate",
    "Bank Passbook",
    "Digital Signature"
  ]

  const [documents, setDocuments] = useState([])

  const fileRef = useRef()

  useEffect(()=>{
    axios.get(`${ serverUrl }doc/data/`, { headers: tokenHeaders })
    .then(res=>{
      setDocuments(res.data)
    })
    .catch(err=>{
      console.log(err)
    })
  }, [file])

  const uploadDocument = (e)=>{
    e.preventDefault()
    let formdata = new FormData()
    formdata.append('profile', id)
    formdata.append('document', file)
    formdata.append('document_name', file.name)
    formdata.append('document_type', type)

    const data = documents.filter((e, i)=>{
      return type === e.document_type
    })

    let url = null;

    if(data && data.length>0){
      axios.put(`${ serverUrl }doc/update-data/${ data[0].id }/`, formdata, { headers: tokenHeaders })
      .then(res=>{
        if(res.data && res.data.status){
          setType('')
          setFile('')
          fileRef.current.value = null
          alert(res.data.message)
        }else {
          alert(res.data.message)
        }
      })
      .catch(err=>{
        console.log(err)
        alert(err)
      })
    }else {
      axios.post(`${ serverUrl }doc/data/`, formdata, { headers: tokenHeaders })
      .then(res=>{
        if(res.data && res.data.status){
          setType('')
          setFile('')
          fileRef.current.value = null
          alert(res.data.message)
        }else {
          alert(res.data.message)
        }
      })
      .catch(err=>{
        console.log(err)
        alert(err)
      })
    }
  }

  return(
    <div className="fixed card" style={{width: "600px", left: "29%"}}>
      <div className="card-header">Documents
        <span className="close" onClick={()=>close()}>&times;</span>
      </div>
      <div className="card-body">
        <h4>Upload Document</h4>
        <form method="POST" onSubmit={ uploadDocument }>
          <div className="form-group">
            <label>File Type</label>
            <select className="form-control" required onChange={ (e)=> setType(e.target.value) } value={ type }>
              <option value="">Select</option>
              {fileTypes.map((e, i)=>(
                <option key={ i }>{e}</option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <label>Select File</label>
            <input type="file" ref={ fileRef } required className="form-control" onChange={ (e)=> {
              if(e.target.files && e.target.files.length>0){
                setFile(e.target.files[0])
              }
            } } />
          </div>
          <div className="form-group">
            <input type="submit" value="Upload" className="btn" />
          </div>
        </form>
        <h4>Documents</h4>
        <div style={{height: '170px', overflow: 'auto'}}>
          <table className="table table-bordered">
            <tbody>
              {documents && documents.length>0?
                documents.map((e, i)=>(
                  <tr key={ i }>
                    <td>{ i + 1 }.</td>
                    <td>{ e.document_type }</td>
                    <td><a href={`${ downloadUrl }${ e.document }`} download >download</a></td>
                  </tr>
                ))
              :null}

            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}
export default DocumentModel
