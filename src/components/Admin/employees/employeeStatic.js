import React, {useState, useEffect} from 'react';
import DataTable from 'react-data-table-component';
import Header from '../subComponents/header';
import Path from '../subComponents/path';
import MainSidebar from '../subComponents/sidebar';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders';
import moment from 'moment';

import CreateAttendenceModel from './createAttendenceModel';
import EditAttendenceModel from './editAttendenceModel';

const initialColumns = [
  {
    name: 'Date',
    selector: 'daydate.date',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{moment(row.daydate.date, "YYYY-MM-DD").format("DD-MMM-YYYY")}</span>
  },
  {
    name: 'Day',
    selector: 'daydate.day',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{row.daydate.day === "Saturday" || row.daydate.day === "Sunday"? <span className="text-danger">{row.daydate.day}</span>: <span>{row.daydate.day}</span>}</span>
  },
  {
    name: 'Day Type',
    selector: 'daydate.day_type',
    sortable: true,
  },
  {
    name: 'In-Time',
    selector: 'in_time',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{row.in_time !== "00:00:00"? moment(row.in_time, "hh:mm:ss").format("hh:mm A"): '-'}</span>
  },
  {
    name: 'Out-Time',
    selector: 'out_time',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{row.out_time !== "00:00:00"? moment(row.out_time, "hh:mm:ss").format("hh:mm A"): '-'}</span>
  },
  {
    name: 'Hour Worked',
    selector: 'hour_worked',
    sortable: true,
    cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{`${row.hour_worked !== "00:00:00"? `${moment(row.hour_worked, "hh:mm:ss").format("hh:mm")} Hrs.`: '-'} `}</span>
  }
]

const badges = [
  "badge-success",
  "badge-warning",
  "badge-danger",
  "badge-primary"
]

const statusTypes = [
  ["PRESENT", "PAID LEAVE", "SICK LEAVE", "CASUAL HOLIDAY"],
  ["INFORMED", "ABS INFORMED", "HALF DAY", "LATE ARRIVAL", "MISS PUNCH", "NONE", "HALF WORKING DAY"],
  ["ABSENT"],
  ["WEEKEND", "PUBLIC HOLIDAY", "FESTIVAL HOLIDAY", "HOLIDAY", "HALF WORKING DAY"]
]

const checkStatus = (status)=>{
  try{
    let st = statusTypes.map((e, i)=>{
      if(e.includes(status)){
        return i
      }
    })
    st = st.filter((e, i)=>{
      return e !==undefined
    })
    return badges[st[0]]
  }catch(e){

  }
}

const EmployeeStatic = ({match})=>{
  const path = [{name: 'Employees', url: '/admin/employees'},
                {name: match.params.name, url: `/admin/employees/${match.params.id}/${match.params.name}/static`},
                {name: 'Static', url: `/admin/employees/${match.params.id}/${match.params.name}/static`}]
  const [columns, setColumns] = useState(initialColumns)
  const [empStatic, setEmpStatic] = useState([])

  const [editModel, setEditModel] = useState(false)
  const [editData, setEditData] = useState('')

  const [createModel, setCreateModel] = useState(false)

  useEffect(()=>{
    setColumns([...columns, {
      name: 'Status',
      selector: 'status_type',
      sortable: true,
      cell: row=> <span onClick={()=>{
          setEditData(row)
          setEditModel(true)
        }} className={`badge ${checkStatus(row.status_type)}`} style={{fontSize: '13px', cursor: 'pointer', position: 'relative', zIndex: '999'}}>{row.status_type}</span>
      }])
  }, [])

  useEffect(()=>{
    axios.get(`${serverUrl}attendance/data-profile/${match.params.id}/`, {headers: tokenHeaders})
    .then(res=>{
      setEmpStatic(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [editModel])

  return(
    <article>
      {createModel?
        <CreateAttendenceModel
          close={()=> setCreateModel(false)}
          id={match.params.id}
        />
      :null}
      {editModel?
        <EditAttendenceModel
          close={()=> setEditModel(false)}
          attendance={editData}
        />
      :null}
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <div>
                    <span style={{color: '#0056b3', cursor: 'pointer'}} onClick={()=> setCreateModel(true)}>Create Attendance</span>
                  </div>
                  <hr/>
                  <div className="row">
                    <div className="col-lg-12 col-md-12">
                      <DataTable
                        paginationRowsPerPageOptions={[10,25,50,100]}
                        pagination={true}
                        columns={columns}
                        data={empStatic}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>
  )
}
export default EmployeeStatic;
