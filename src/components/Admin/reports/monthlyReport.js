import React, {useState, useEffect} from 'react';
import Header from '../subComponents/header';
import Path from '../subComponents/path';
import MainSidebar from '../subComponents/sidebar';
import DataTable from 'react-data-table-component';
import {NavLink} from 'react-router-dom';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders';
import moment from 'moment';

const columns = [
  {
    name: 'Name',
    selector: 'profile_name',
    sortable: true,
  },
  {
    name: 'Code',
    selector: 'profile_code',
    sortable: true,
  },
  {
    name: 'PRESENT',
    selector: 'PRESENT',
    sortable: true,
  },
  {
    name: 'LATE ARRIVAL',
    selector: 'LATE_ARRIVAL',
    sortable: true,
  },
  {
    name: 'CASUAL LEAVE',
    selector: 'CASUAL_LEAVE',
    sortable: true,
  },
  {
    name: 'SICK LEAVE',
    selector: 'SICK_LEAVE',
    sortable: true,
  },
  {
    name: 'ABSENT',
    selector: 'ABSENT',
    sortable: true,
  },
  {
    name: 'HALF DAY',
    selector: 'HALF_DAY',
    sortable: true,
  },
  {
    name: 'MISS PUNCH',
    selector: 'MISS_PUNCH',
    sortable: true,
  },
  {
    name: 'NONE',
    selector: 'NONE',
    sortable: true,
  }
]

const generateYears = ()=>{
  let years = [];
  const date = parseInt(moment().format('YYYY'))
  for(let i=-5; i < 5; i++){
    years.push(date + i)
  }
  return years;
}
const generateMonths = ()=>{
  let months = [];
  for(let i=1; i < 13; i++){
    months.push(i)
  }
  return months;
}

const MonthlyReport = ({match})=>{
  const path = [{name: 'Monthly Report', url: '/admin/monthly-report'}]
  const years = generateYears()
  const months = generateMonths()
  const [month, setMonth] = useState('')
  const [year, setYear] = useState('')
  const [report, setReport] = useState([])
  const [totalReport, setTotalReport] = useState([])

  useEffect(()=>{
    const date = moment()
    const m = date.format('MM')
    const y = date.format('YYYY')
    setMonth(m)
    setYear(y)
  }, [])

  useEffect(()=>{
    if(month && year){
      axios.get(`${serverUrl}attendance/data-by-month/${month}/${year}/`, {headers: tokenHeaders})
      .then(res=>{
        setReport(res.data.month_static)
        setTotalReport(res.data.calender_activity)
      })
      .catch(err=>{
        console.log(err);
      })
    }
  }, [month, year])

  return(
    <article>
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <div>
                    <table>
                      <tbody>
                        <tr>
                          <td>
                            <label>Month</label>
                            <select className="form-control" onChange={(e)=> setMonth(e.target.value)} value={month}>
                                <option value="">Select</option>
                              {months && months.length>0?
                                months.map((e, i)=>{
                                  if(e<10){
                                    return(
                                      <option key={i} value={`0${e}`}>{moment(e, "MM").format("MMMM")}</option>
                                    )
                                  }else {
                                    return(
                                      <option key={i} value={e}>{moment(e, "MM").format("MMMM")}</option>
                                    )
                                  }
                                })
                              :null}
                            </select>
                          </td>
                          <td>
                            <label>Year</label>
                            <select className="form-control" onChange={(e)=> setYear(e.target.value)} value={year}>
                                <option value="">Select</option>
                              {years && years.length>0?
                                years.map((e, i)=>(
                                  <option key={i}>{e}</option>
                                ))
                              :null}
                            </select>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <hr/>
                  </div>
                  <ReportCard
                    title={`Monthly Employees Report:- ${moment(month+"-"+year, "MM-YYYY").format("MMMM YYYY")}`}
                    columns={columns}
                    report={report}
                    totalReport={totalReport}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>

  )
}

const ReportCard = ({ title, columns, report , totalReport})=>{
  const { WORKING, HOLIDAY, PUBLIC_HOLIDAY, WEEKEND, HALF_WORKING_DAY } = totalReport;
  return(
    <div className="card bg-light">
      <div className="card-header">{title}</div>
      <div className="card-body">
        <DataTable
          columns={columns}
          data={report}
        />
      </div>
      <div className="card-footer report-table-container">
        <table className="report-table" style={{width: "100%"}}>
          <thead>
            <tr>
              <th>WORKING</th>
              <th>HOLIDAY</th>
              <th>PUBLIC HOLIDAY</th>
              <th>WEEKEND</th>
              <th>HALF WORKING DAY</th>
            </tr>
          </thead>
          <tbody>
            <tr>
            <td>{WORKING}</td>
            <td>{HOLIDAY}</td>
            <td>{PUBLIC_HOLIDAY}</td>
            <td>{WEEKEND}</td>
            <td>{HALF_WORKING_DAY}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default MonthlyReport;
