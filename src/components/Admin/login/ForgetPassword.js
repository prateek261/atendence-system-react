import React, {useState} from 'react';
import {NavLink} from 'react-router-dom';
import axios from 'axios';
import {successAlert, errorAlert} from '../utils/alert';
import Loader from '../utils/loader';
import logo from '../../../assets/img/logo.png';
import {serverUrl, headers} from '../utils/ajaxHeaders';

const ForgetPassword = ({match})=>{
  const [email, setEmail] = useState('')
  const [loader, setLoader] = useState(false)

  const formSubmit = async (e)=>{
    e.preventDefault();
    setLoader(true)
    const payload = {
      email: email,
    }

    const json = await axios.post(`${ serverUrl }account-sign/company-login/`, payload, {headers: headers});
    if(json && json.data && json.data.message==="logged in"){
      localStorage.setItem('token', json.data.token)
      setEmail('')
      setLoader(false)
      window.location.href= '/'
    }else if(json && json.message){
      alert(json.message)
    }else{
      alert('try again')
    }
    setLoader(false)
  }
  return(
      <article>
      {loader?
        <Loader />
      :null}
          <section className="sectionColumns">
            <div className="leftColumn">
                <div className="content-leftColumn">
                  <div className="columnImg" style={{padding: '20%'}}>
                    <img src={logo} alt="sideimg"/>
                  </div>
                    <div className="container">

                    </div>
                </div>
            </div>
              <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4 loginHeader">Forget Password</h5>
                      <div className="boxComman">
                          <form onSubmit={formSubmit}>
                              <div className="form-group">
                                  <label>Email address</label>
                                  <input type="email" className="form-control" required onChange={(e)=>{
                                    setEmail(e.target.value)
                                  }} value={email} placeholder="Enter Email Address"/>
                              </div>
                              <div className="row align-items-center">
                                  <div className="col-md-6">
                                      <p className="mb-0"><NavLink to="/" title="Login" className="font-weight-medium loginColor"> Login</NavLink> </p>
                                  </div>
                                  <div className="col-md-6 text-right">
                                      <input type="submit" title="Submit" value="Submit"/>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
}

export default ForgetPassword;
