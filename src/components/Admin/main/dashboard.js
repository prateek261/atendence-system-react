import React, {useState, useEffect} from 'react';
import Header from '../subComponents/header';
import Path from '../subComponents/path';
import MainSidebar from '../subComponents/sidebar';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../utils/ajaxHeaders';

const Dashboard = ({match})=>{
  const path = [{name: 'Dashboard', url: '/admin/dashboard'}]
  const [dashboard, setDashboard] = useState({lr: 0, })

  useEffect(()=>{
    axios.get(`${serverUrl}profile/data/`, {headers: tokenHeaders})
    .then(res=>{
      setDashboard(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  return(
    <article>
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <div className="row">
                    <div className="col-lg-12 col-md-12">
                      <div className="row">
                        <div className="col-lg-4 col-md-4">
                          <div className="card bg-warning text-white">
                            <div className="card-body">
                              <h2 className="card-text">{dashboard.lr}</h2>
                              <h4 className="card-text" style={{paddingBottom: "0px"}}>Pending Leave Request</h4>
                            </div>
                            <div className="card-footer"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>
  )
}
export default Dashboard;
