import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../../utils/ajaxHeaders';
import '../../../../assets/css/fixed.css';
import moment from 'moment';

const initial = [
  {
    day_type: '',
    date: '',
    day: ''
  }
]

const AssignActivity = ({event, close})=>{
  const [activity, setActivity] = useState(initial)
  const [title, setTitle] = useState('')
  const [activityType, setActivityType] = useState([])

  const createRange = (startDate, stopDate, days)=>{
    let dateArray = [];
    let currentDate = moment(startDate);
    stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        const d = currentDate.format('d')
        dateArray.push({
          day_type: '',
          date: moment(currentDate).format('YYYY-MM-DD'),
          day: days[d][0]
        })
        currentDate = moment(currentDate).add(1, 'days');
    }
    console.log(dateArray);
    return dateArray;
  }
  useEffect(()=>{
    axios.get(`${serverUrl}configure/choice/DAY_CHOICES/`, {headers: tokenHeaders})
    .then(res=>{
      const range = createRange(event.start, event.end, res.data)
      setActivity(range)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  useEffect(()=>{
    axios.get(`${serverUrl}configure/choice/DAY_TYPE/`, {headers: tokenHeaders})
    .then(res=>{
      setActivityType(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  const createActivity = (e)=>{
    e.preventDefault()
    let payload = [];
    activity.map((e, i)=>{
      payload.push({
        ...e,
        day_type: title
      })
    })
    console.log(payload);
    axios.post(`${serverUrl}daydate/date-data/`, payload, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        alert(res.data.message)
        close()
      }else {
        alert(res.data.message)
      }
    })
    .catch(err=>{
      console.log(err);
    })
  }
  return(
    <div className="fixed card">
      <form method="POST" onSubmit={createActivity}>
        <div className="card-header">Assign Activity
          <span className="close" onClick={()=>close()}>&times;</span>
        </div>
        <div className="card-body">
          <div className="form-group">
            <label>Title</label>
            <select required value={title} onChange={(e)=>{
              setTitle(e.target.value)
            }} className="form-control">
              <option value="">Select</option>
              {activityType && activityType.length>0?
                activityType.map((e, i)=>(
                  <option key={i}>{e[0]}</option>
                ))
              :null}
            </select>
          </div>
          <div className="form-group">
            <input type="submit" value="Create Activity" className="btn" />
          </div>
        </div>
      </form>
    </div>
  )
}
export default AssignActivity;
