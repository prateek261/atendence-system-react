import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../../utils/ajaxHeaders';
import '../../../../assets/css/fixed.css';
import moment from 'moment';


const EditActivity = ({event, close, list})=>{
  const [activity, setActivity] = useState('')
  const [activityType, setActivityType] = useState([])
  const [id, setId] = useState('')

  useEffect(()=>{
    const res = list.filter((e, i)=>{
      return moment(e.start).isSame(event.start)
    })
    if(res && res.length>0){
      setId(res[0].id)
    }

    setActivity(event.title)
    axios.get(`${serverUrl}configure/choice/DAY_TYPE/`, {headers: tokenHeaders})
    .then(res=>{
      setActivityType(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  const updateActivity = (e)=>{
    e.preventDefault()
    const payload= {
      day_type: activity
    }
    axios.put(`${serverUrl}daydate/update-date-data/${id}/`, payload, {headers: tokenHeaders})
    .then(res=>{
      if(res.data && res.data.status){
        alert(res.data.message)
        close()
      }else {
        alert(res.data.message)
      }
    })
    .catch(err=>{
      console.log(err);
    })
  }
  return(
    <div className="fixed card">
      <form method="POST" onSubmit={updateActivity}>
        <div className="card-header">Edit Activity
          <span className="close" onClick={()=>close()}>&times;</span>
        </div>
        <div className="card-body">
          <div className="form-group">
            <label>Title</label>
            <select required value={activity} onChange={(e)=>{
              setActivity(e.target.value)
            }} className="form-control">
              <option value="">Select</option>
              {activityType && activityType.length>0?
                activityType.map((e, i)=>(
                  <option key={i}>{e[0]}</option>
                ))
              :null}
            </select>
          </div>
          <div className="form-group">
            <input type="submit" value="Update Activity" className="btn" />
          </div>
        </div>
      </form>
    </div>
  )
}
export default EditActivity;
