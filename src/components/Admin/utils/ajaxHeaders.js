export const serverUrl= "http://192.168.1.15:8000/"
export const downloadUrl= "http://192.168.1.15:8000"

export const tokenHeaders = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  'Authorization': 'Token '+localStorage.getItem("token")
}

export const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
}

export const getId = ()=>{
  try {
    let user = localStorage.getItem('user')
    user = JSON.parse(user)
    return user.data.id
  } catch (e) {
    return null
  }
}
