import React, {Component} from 'react';
import DataTable from 'react-data-table-component';

class Datatable extends Component {
  constructor(){
    super();
    this.state = {
      data: [],
      columns: []
     }
  }
  componentWillMount(){
    this.setState({
      data: this.props.data,
      columns: this.props.columns
    });
  }

  handleChange = (state) => {
    this.props.returnFunc(state.selectedRows);
  };
  render() {
    if(this.props.selectableRows){
      return (
        <div style={{"marginTop":"32px"}}>
        <DataTable
          paginationRowsPerPageOptions={[10,25,50,100]}
          pagination={true}
          columns={this.state.columns}
          data={this.state.data}
          selectableRows
          onTableUpdate={this.handleChange}
        />
        </div>
      )
    }else{
      return (
        <div style={{"marginTop":"32px"}}>
        <DataTable
          paginationRowsPerPageOptions={[10,25,50,100]}
          pagination={true}
          columns={this.state.columns}
          data={this.state.data}
          onTableUpdate={this.handleChange}
        />
        </div>
      )
    }
  }
}

export default Datatable;
