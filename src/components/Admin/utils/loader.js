import React from "react";

export const Loader = ()=>{
  return(
    <div className="loadingPage">
      <div className="loader"></div>
    </div>
  )
}

export default Loader;
