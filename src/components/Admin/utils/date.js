import React from 'react';
import moment from 'moment';

const createDate = (date)=>{
  let data = moment(date, 'YYYY-MM-DDTHH:mm:ssZ').utc().format("DD/MM/YY hh:mm A");
    data = data.split(' ')
  return {
    today: data[0],
    time: data[1]+" "+data[2]
  }
}

export const generateETA = (eta)=>{
  try{
    if(eta === 'No Route Found'){
      return (
        <span className="text-danger">No Route Found</span>
      )
    }
  let diffDays = Math.floor(eta / 86400);
  let diffHrs = Math.floor((eta % 86400) / 3600);
  let diffMins = Math.round(((eta % 86400) % 3600) / 60);
  let diffSec = Math.round(((eta % 86400) % 3600) / 60);
  let remaing = '';
  if(diffDays>1){
    remaing += diffDays + " days, ";
  }else if(diffDays===1){
    remaing += diffDays + " day, ";
  }

  if(diffHrs>1){
    remaing += diffHrs + " hours, ";
  }else if(diffHrs===1){
    remaing += diffHrs + " hour, ";
  }
  if(diffMins>1){
    remaing += diffMins + " minutes ";
  }else if(diffMins===1){
    remaing += diffMins + " minute ";
  }
  if(diffSec>1){
    remaing += diffSec + " seconds ";
  }else if(diffSec===1){
    remaing += diffSec + " second ";
  }
  return (
    remaing
  )
  }catch(e){
    return '-'
  }
}

export const formateDate =(date)=>{
  if(date){
    let d = createDate(date);
    return (
      <span>{d.today}<br/>{d.time}</span>
    )
  }
  return (
    ''
  )
}

export const formateDate2 =(date)=>{
  if(date){
    let d = createDate(date);
    let datetime = d.today+" "+d.time;
    return (
      datetime
    )
  }
  return (
    ''
  )
}

export const formateDate3 =(date)=>{
  if(date){
    let data = moment(date, 'YYYY-MM-DDTHH:mm:ssZ').utc().format("DD/MM/YY hh:mm A");
    return (
      data
    )
  }
  return (
    ''
  )
}

export const formateDate4 =(date)=>{
  if(date){
    let data = moment(date, 'YYYY-MM-DDTHH:mm:ssZ').format("DD/MM/YY hh:mm A");
    return (
      data
    )
  }
  return (
    ''
  )
}

export const formateDate5 =(date)=>{
  if(date){
    let data = moment(date, 'YYYY-MM-DDTHH:mm:ssZ').format("DD/MM/YY");
    return (
      data
    )
  }
  return (
    ''
  )
}

export const formateDateEn =(date)=>{
  if(date){
    let data = date.format("DD/MM/YY hh:mm A");
    return (
      data
    )
  }
  return (
    ''
  )
}


export const formateTime =(date)=>{
  if(date){
    let time = moment(date, 'YYYY-MM-DDTHH:mm:ssZ').utc().format("hh:mm A");
    return (
      time
    )
  }
  return (
    ''
  )
}


export const formateNextTime =(date)=>{
  if(date){
    date = new Date(date);
    let min=date.getMinutes();
    date.setMinutes(min+30);
    min=date.getMinutes();
    if(date.getMinutes()<10){
      min="0"+date.getMinutes();
    }
    let hours = date.getHours();
    if(hours>12){
      let time = Math.abs(hours-12)+":"+min;
      return (
        time
      )
    }else{
      let time = hours+":"+min;
      if(hours===0){
        time = "12:"+min;
      }
      return (
        time
      )
    }
  }
  return (
    ''
  )
}

export const convertToISO = (date)=>{
  try{
    date = date.split(' ');
    let newDate = date[0].split('/');

    let newTime = date[1].split(':');
    let hours = Math.abs(Number(newTime[0])-24);
    let d = new Date(newDate[2], newDate[1], newDate[0], hours, newTime[1]);
    return d.toISOString();
  }catch(e){
    return date;
  }
}

export const dateDiff = (date1, date2, date3)=>{
  let remaing = '';
//  let to_date = new Date(date1);
//.format("YYYY-MM-DD hh:mm A");
  let from_date = moment(date3, 'YYYY-MM-DDTHH:mmZ').utc();
  let to_date = moment(date1, 'YYYY-MM-DDTHH:mmZ').utc();
  let actual = moment(date2, 'YYYY-MM-DDTHH:mmZ').utc();
//  let actual = new Date(date2);

  if(actual.diff(to_date, 'minutes')>0){
    let diffMs = moment.duration(actual.diff(to_date));
    //(actual - to_date);
    let diffDays = Math.floor(diffMs / 86400000);
    let diffHrs = Math.floor((diffMs % 86400000) / 3600000);
    let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);

    if(diffDays>1){
      remaing += diffDays + " days, ";
    }else if(diffDays===1){
      remaing += diffDays + " day, ";
    }

    if(diffHrs>1){
      remaing += diffHrs + " hours, ";
    }else if(diffHrs===1){
      remaing += diffHrs + " hour, ";
    }
    if(diffMins>1){
      remaing += diffMins + " minutes ";
    }else if(diffMins===1){
      remaing += diffMins + " minute ";
    }
    return (
      <span style={{"color":"#7a7c75"}}>
        <span style={{"color":"black"}}>Delay: </span>
      {remaing}</span>
    )
  }else if(!date2){
    return(
      ''
    )
  }else if(from_date >= actual && actual <= to_date){
    return (
      <span style={{"color":"#01d040"}}>Before Time</span>
    )
  }
  return (
    <span style={{"color":"#01d040"}}>On Time</span>
  )
}

export const checkDate = (date)=>{
  let c = moment().subtract(1, "days");
  if(c<date){
    return true;
  }
}

export const  getCurrentTS = ()=>{
  return moment().format('DD/MM/YY hh:mm A');
}
