import PostFetch from '../ajax/postFetch';

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
};

export const validateEmail = async (email)=>{
  try {
    if(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,3})+$/.test(email)){
      const payload = {
        email: email
      }
      const data = await PostFetch('CheckEmail', payload, headers);
      if(data && data.status){
        return {
          status: true,
          error: ''
        }
      }else {
        return {
          status: false,
          error: 'Already associated with '+ data.message
        }
      }
    }else {
      return {
        status: false,
        error: 'This email is not valid'
      }
    }
  } catch (e) {
    return {
      status: false,
      error: 'Email is required'
    }
  }
}

export const phoneValidation = async (phone)=>{
  try {
    if(/^\d{8}$/.test(phone)){
      const payload = {
        phone: phone
      }
      const data = await PostFetch('CheckPhone', payload, headers);
      if(data && data.status){
        return {
          status: true,
          error: ''
        }
      }else {
        return {
          status: false,
          error: 'Already associated with '+ data.message
        }
      }
    }else {
      return {
        status: false,
        error: 'This phone number is not valid'
      }
    }
  } catch (e) {
    return {
      status: false,
      error: 'Phone number is required'
    }
  }
}
