import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../../utils/ajaxHeaders';
import './email.css';

import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {stateToHTML} from 'draft-js-export-html';

const initial = {
  start_date: '',
  end_date: '',
  request_type: '',
  body: EditorState.createEmpty()
}

const EmailModel = ({email, close, title})=>{
  const [request, setRequest] = useState(initial)
  const [requestType, setRequestType] = useState([])
  useEffect(()=>{
    axios.get(`${serverUrl}configure/choice/REQUEST_TYPE/`, {headers: tokenHeaders})
    .then(res=>{
      setRequestType(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }, [])

  const sendEmail = (e)=>{
    e.preventDefault()

    axios.post(`${serverUrl}`, request, {headers: tokenHeaders})
    .then(res=>{
      // if(res.data && res.data.status){
      //   alert(res.data.message)
      //   close()
      // }else {
      //   alert(res.data.message)
      // }
    })
    .catch(err=>{
      console.log(err);
    })
  }
  return(
    <div className="fixedEmail card">
      <form method="POST">
        <div className="card-header">{title}
          <span className="close" onClick={()=>close()}>&times;</span>
        </div>
        <div className="card-body">
          <div className="form-group">
            <label>Subject</label>
            <select onChange={(e)=> setRequest({
              ...request,
              request_type: e.target.value
            })} value={request.request_type} className="form-control">
              <option value="">Select</option>
              {requestType && requestType.length>0?
                requestType.map((e, i)=>(
                  <option>{e}</option>
                ))
              :null}
            </select>
          </div>
          <div className="form-group">
            <label>Body</label>
            <Editor
              editorState={request.body}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              onEditorStateChange={(editorState)=> {
                console.log(editorState);
                setRequest({
                  ...request,
                  body: editorState
                })
              }}
            />
          </div>
          <div className="form-group">
            <input type="submit" value="Send" className="btn" />
            <button className="btn" onClick={()=> close()} style={{marginLeft: '15px'}}>Close</button>
          </div>
        </div>
      </form>
    </div>
  )
}
export default EmailModel;
