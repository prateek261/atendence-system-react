import React, {useState, useEffect} from 'react';
import DataTable from 'react-data-table-component';
import Header from '../../subComponents/header';
import Path from '../../subComponents/path';
import MainSidebar from '../../subComponents/sidebar';
import axios from 'axios';
import {serverUrl, tokenHeaders} from '../../utils/ajaxHeaders';
import moment from 'moment';
import Loader from '../../utils/loader';

import EmailModel from '../subComponents/email';

const LeaveRequests = ({match})=>{
  const path = [{name: 'Requests', url: '/admin/requests/leave'},
                {name: 'Leave', url: '/admin/requests/leave'}]
  const [requests, setRequests] = useState([])
  const [emailModel, setEmailModel] = useState(false)
  const [email, setEmail] = useState('')

  const [loader, setLoader] = useState(false)

  const columns = [
    {
      name: 'Name',
      selector: 'profile.first_name',
      sortable: true,
      cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{`${ row.profile.first_name.toUpperCase() } ${ row.profile.middle_name? row.profile.middle_name.toUpperCase() : '' } ${ row.profile.last_name.toUpperCase() }`}</span>
    },
    {
      name: 'Start Date time',
      selector: 'start_date',
      sortable: true,
      cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{moment(row.start_date, "YYYY-MM-DD hh:mm").format("DD-MMM-YYYY hh:mm A")}</span>
    },
    {
      name: 'End Date time',
      selector: 'end_date',
      sortable: true,
      cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{moment(row.end_date, "YYYY-MM-DD hh:mm").format("DD-MMM-YYYY hh:mm A")}</span>
    },
    {
      name: 'Request Type',
      selector: 'request_type',
      sortable: true,
    },
    {
      name: 'Accepted',
      selector: 'accepted',
      sortable: true,
      cell: row=> <span style={{position: 'relative', zIndex: '999', fontSize: '13px'}} className={`badge ${row.accepted === 'ACCEPTED'? "badge-success" : row.accepted === 'PENDING'? "badge-primary": "badge-danger"}`}>{row.accepted}</span>
    },
    {
      name: 'Action',
      selector: 'accepted',
      sortable: true,
      cell: row=> <span style={{position: 'relative', zIndex: '999'}}>
          <React.Fragment>
            {row.accepted === 'PENDING' || row.accepted !== 'ACCEPTED'?
            <i onClick={ ()=> acceptRequest('ACCEPTED', row.id) } style={{margin: '5px', fontSize: '18px', cursor: 'pointer'}} class="fas fa-check text-success"></i>
            :null}
            {row.accepted === 'PENDING' || row.accepted !== 'REJECTED'?
            <i onClick={ ()=> acceptRequest('REJECTED', row.id) } style={{margin: '5px', fontSize: '18px', cursor: 'pointer'}} class="fas fa-times text-danger"></i>
            :null}
          </React.Fragment>
      </span>
    },
  ]

  const acceptRequest = (status, id)=>{
    setLoader(true)
    axios.put(`${serverUrl}request/data/request/${ id }/`, {accepted: status}, {headers: tokenHeaders})
    .then(res=>{
      getAllRequests()
      setLoader(false)
      alert(res.data.message)
    })
    .catch(err=>{
      setLoader(false)
      alert('try again')
      console.log(err);
    })
  }

  useEffect(()=>{
    getAllRequests()
  }, [])

  const getAllRequests = ()=>{
    axios.get(`${serverUrl}request/data/`, {headers: tokenHeaders})
    .then(res=>{
      setRequests(res.data)
    })
    .catch(err=>{
      console.log(err);
    })
  }

  return(
    <article>
      {emailModel?
        <EmailModel email={email} close={()=> setEmailModel(false)}/>
      :null}
      {loader?
        <Loader />
      :null}
      <div className="dashboardMain">
        <Header />
        <div className="dashboardContent">
          <div className="rightColumn">
            <MainSidebar/>
            <div className="rightColumnContent">
              <Path path={path}/>
              <div className="boxContentInner">
                <div className="contentAccountSetting">
                  <div className="row">
                    <div className="col-lg-12 col-md-12">
                      <DataTable
                        paginationRowsPerPageOptions={[10,25,50,100]}
                        pagination={true}
                        columns={columns}
                        data={requests}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>

  )
}
export default LeaveRequests;
