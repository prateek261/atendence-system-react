import React from 'react'
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import './App.css'
import RoutePage from './components/RoutePage'
import Login from './components/Admin/login/login'
import Logout from './components/Admin/login/logout'
import ForgetPassword from './components/Admin/login/ForgetPassword'
import NotFound from './components/Admin/login/notFound'
import Dashboard from './components/Admin/main/dashboard'
import BigCalendar from './components/Admin/main/calendar/bigCalendar'
import Employees from './components/Admin/employees/employees'
import EmployeeDetails from './components/Admin/employees/employeeDetails'
import EditDetailsAdmin from './components/Admin/employees/EditEmployeeDetails'
import EmployeeStatic from './components/Admin/employees/employeeStatic'
import MonthlyReport from './components/Admin/reports/monthlyReport'
import LeaveRequests from './components/Admin/requests/leaves/leaveRequests'

import EmpLogin from './components/Employee/login/login'
import EmpLogout from './components/Employee/login/logout'
import EmpDashboard from './components/Employee/main/dashboard'
import EmpBigCalendar from './components/Employee/main/calendar/bigCalendar'
import EmpDetails from './components/Employee/employee/employeeDetails'
import EditDetails from './components/Employee/employee/EditDetails'
import EmpMonthlyReport from './components/Employee/reports/monthlyReport'
import EmpLeaveRequests from './components/Employee/requests/leaves/leaveRequests'

const App = ()=>{
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route path="/" component={(props)=> <RoutePage {...props} />} exact/>
          <Route path="/admin" component={(props)=> <Login {...props} />} exact/>
          <Route path="/forgetpassword" component={(props)=> <ForgetPassword {...props} />} exact/>
          <Route path="/admin/logout" component={(props)=> <Logout {...props} />} exact/>
          <Route path="/admin/dashboard" component={(props)=> <Dashboard {...props} />} exact/>
          <Route path="/admin/calendar" component={(props)=> <BigCalendar {...props} />} exact/>
          <Route path="/admin/employees" component={(props)=> <Employees {...props} />} exact/>
          <Route path="/admin/employees/:id" component={(props)=> <EmployeeDetails {...props} />} exact/>
          <Route path="/admin/employees/:id/edit" component={(props)=> <EditDetailsAdmin {...props} />} exact/>
          <Route path="/admin/employees/:id/:name/static" component={(props)=> <EmployeeStatic {...props} />} exact/>
          <Route path="/admin/monthly-report" component={(props)=> <MonthlyReport {...props} />} exact/>
          <Route path="/admin/requests/leave" component={(props)=> <LeaveRequests {...props} />} exact/>

          <Route path="/employee" component={(props)=> <EmpLogin {...props} />} exact/>
          <Route path="/employee/logout" component={(props)=> <EmpLogout {...props} />} exact/>
          <Route path="/employee/dashboard" component={(props)=> <EmpDashboard {...props} />} exact/>
          <Route path="/employee/calendar" component={(props)=> <EmpBigCalendar {...props} />} exact/>
          <Route path="/employee/details" component={(props)=> <EmpDetails {...props} />} exact/>
          <Route path="/employee/details/edit" component={(props)=> <EditDetails {...props} />} exact/>
          <Route path="/employee/monthly-report" component={(props)=> <EmpMonthlyReport {...props} />} exact/>
          <Route path="/employee/requests/leave" component={(props)=> <EmpLeaveRequests {...props} />} exact/>

          <Route path="*" component={(props)=> <NotFound {...props} />} exact/>
        </Switch>
      </BrowserRouter>
    </div>
  )
}

export default App
